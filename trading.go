package sheeldmarket

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

//AddDMAOrder Direct market access (DMA) orders are executed instantly on a single exchange.
// Here are the available DMA order types:
// IoC, Immediate or Cancel: Executed, partially or wholly, when placed into the system. Any non-executed quantity is immediately cancelled.
// GTC, Good Till Cancel: Any unexecuted portion of the order, will remain in effect until executed, cancelled by the entering party, or expiration.
// Day order: Same as a GTC order but automatically cancelled at the end of the trading session.
// POD, Post-Only Day order: Provide liquidty to the market without the guarantee not to take existing liquidty.
//see: https://docs.trade.sheeldmatch.com/#add-a-dma-order
func (s *SheeldMarket) AddDMAOrder(orderType DMAOrderType, val AddDMAOrderRequest) error {
	body, err := json.Marshal(val)
	if err != nil {
		return fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"participants/"+s.cnf.MPID+"/orders/dma/"+orderType.String(), bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}

//AddSOROrder Smart Order Routing (SOR) orders are working across several exchanges in order to optimise your execution to get the best price.
// Here are the available SOR order types:
// ISO, Intermarket Sweep Order: Intends to fill the full quantity in one shot across all venues we are connected to. Any non-executed quantity remaining during this only attempt is immediately cancelled.
// IDO, Intermarket Day Order: Same characteristics as an ISO, except it stays open until full quantity has been filled.
// IGO: Intermarket Good Till Cancel Order: Combination between an IDO and a GTC order.
// IPO: Intermarket Post-Only Order: Like an IDO but is always maker (never taker)
//see: https://docs.trade.sheeldmatch.com/#add-a-sor-order
func (s *SheeldMarket) AddSOROrder(orderType SOROrderType, val AddSOROrderRequest) error {
	body, err := json.Marshal(val)
	if err != nil {
		return fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"participants/"+s.cnf.MPID+"/orders/sor/"+orderType.String(), bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}

//AddDSAOrder Direct strategy access (DSA) orders are conductors, which are going to slice orders into smaller parcels of SOR orders, with the aim of minimising market impact, maximising execution price and fill rate. Here are the available DSA order types:
// VIO, Volume Inline Order: Executed by participating to a certain % of the overall volume traded in the different exchanges
// TWAP, Time-Weighted Average Price Order: Executed according to the TWAP during a specified duration
// VWAP, Volume-Weighted Average Price Order: Executed according to the VWAP during a specified duration.
//see: https://docs.trade.sheeldmatch.com/#add-a-dsa-order
func (s *SheeldMarket) AddDSAOrder(orderType DSAOrderType, val AddDSAOrderRequest) error {
	body, err := json.Marshal(val)
	if err != nil {
		return fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"participants/"+s.cnf.MPID+"/orders/dsa/"+orderType.String(), bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}

//AddTMOOrder Tailor-made orders (TMO) designed to answer a client’s specific needs.
// Here are the available TMO order types:
// PTO, Program Trading Order: Executed on synthetic instruments.
// VTO, Volume Triggered Order: Executed when a certain volume has been reached on the different exchanges. Need to specify the amount of volume after which execution will start. Child order is an IGO.
// SLO, Stop Loss Order: Executed to limit the loss of a position until a specified value of an anchor price.
// TPO, Take Profit Order: Executed to capture the profit of a position when an anchor price reaches a specified value.
//see: https://docs.trade.sheeldmatch.com/#add-a-tmo-order
func (s *SheeldMarket) AddTMOOrder(orderType TMOOrderType, val AddTMOOrderRequest) error {
	body, err := json.Marshal(val)
	if err != nil {
		return fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"participants/"+s.cnf.MPID+"/orders/tmo/"+orderType.String(), bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}

//ModifyOrder This method allows market participants to chage the price and/or the size of an existing order.
// If the modification is only a reduction of the order size, the order keeps its place in the price-level queue. Otherwise, the order goes back to the end of the queue.
//see: https://docs.trade.sheeldmatch.com/#modify-an-order
func (s *SheeldMarket) ModifyOrder(token string, val ModifyOrderRequest) error {
	body, err := json.Marshal(val)
	if err != nil {
		return fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPatch, s.baseUrl+"participants/"+s.cnf.MPID+"/orders/"+token+"/modify", bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}

//ReduceOrder This method allows market participants to reduce the volume of an order without it going back to the end of the queue.
// This method requires you provide a volume lower than the current ordered one.
//see: https://docs.trade.sheeldmatch.com/#reduce-an-order
func (s *SheeldMarket) ReduceOrder(token string, val ReduceOrderRequest) error {
	body, err := json.Marshal(val)
	if err != nil {
		return fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPatch, s.baseUrl+"participants/"+s.cnf.MPID+"/orders/"+token+"/reduce", bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}

//GetOrderStatus This method allows market participants to retrieve the status of a single order using its token as identifier
//see: https://docs.trade.sheeldmatch.com/#get-order-status
func (s *SheeldMarket) GetOrderStatus(token string) (*Order, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"participants/"+s.cnf.MPID+"/orders/"+token, nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(Order)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetOpenOrders This method allows market participants to retrieve all open orders for a Market Participant
//see: https://docs.trade.sheeldmatch.com/#get-open-orders
func (s *SheeldMarket) GetOpenOrders() ([]Order, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"participants/"+s.cnf.MPID+"/orders/open", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := make([]Order, 0)
	err = s.do(req, &resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetOrders This method allows market participants to retrieve all orders for a Market Participant.
//see: https://docs.trade.sheeldmatch.com/#get-orders
func (s *SheeldMarket) GetOrders(val GetOrdersRequest) (*GetOrdersResponse, error) {
	// u, _ := url.Parse(path)
	// //q := u.Query()
	// //q.Set("page_size", fmt.Sprintf("%d", maxPageSize))
	// //u.RawQuery = q.Encode()
	path := s.baseUrl + "participants/" + s.cnf.MPID + "/orders"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.Instrument != nil {
		q.Set("instrument", *val.Instrument)
	}
	if val.Side != nil {
		q.Set("side", val.Side.String())
	}
	if val.HideClosed != nil {
		q.Set("hideClosed", fmt.Sprintf("%t", *val.HideClosed))
	}
	u.RawQuery = q.Encode()

	// TODO - pagination
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetOrdersResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//CancelOrder This method allows market participants to cancel one order
//see: https://docs.trade.sheeldmatch.com/#cancel-an-order
func (s *SheeldMarket) CancelOrder(token string) error {
	req, err := http.NewRequest(http.MethodDelete, s.baseUrl+"participants/"+s.cnf.MPID+"/orders/"+token, nil)
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}

//GetExecutions This route allows market participants to retrieve all executions
//see: https://docs.trade.sheeldmatch.com/#get-executions
func (s *SheeldMarket) GetExecutions() (*GetExecutionsResponse, error) {
	// TODO - pagination
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"participants/"+s.cnf.MPID+"/executions", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetExecutionsResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//MassCancelOrders This method mass cancels all open orders of a Market Participant.
//see: https://docs.trade.sheeldmatch.com/#mass-cancel-orders
func (s *SheeldMarket) MassCancelOrders() error {
	req, err := http.NewRequest(http.MethodDelete, s.baseUrl+"participants/"+s.cnf.MPID+"/orders", nil)
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}

//SuspendTrading This method suspends a Market Participant. Its following trading messages will be rejected by the gateway.
//see: https://docs.trade.sheeldmatch.com/#suspend-trading
func (s *SheeldMarket) SuspendTrading() error {
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"participants/"+s.cnf.MPID+"/suspend", nil)
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}

//SuspendTrading This method resumes a Market Participant. Its following trading messages will be accepted by the gateway.
//see: https://docs.trade.sheeldmatch.com/#resume-trading
func (s *SheeldMarket) ResumeTrading() error {
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"participants/"+s.cnf.MPID+"/resume", nil)
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}
