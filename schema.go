package sheeldmarket

import (
	"fmt"
	"time"
)

type SheeldMarketConfig struct {
	Test     bool
	Username string
	Password string
	MPID     string
}

type AuthToken struct {
	AuthToken string `json:"authToken"`
}

type APIResponse string

type APIError struct {
	Err      ErrorBody `json:"error"`
	Message  *string   `json:"message"`
	Title    *string   `json:"title"`
	Resource *string   `json:"resource"`
}

type ErrorBody struct {
	Status  int64  `json:"status"`
	Message string `json:"message"`
}

func (e *APIError) Error() string {
	return fmt.Sprintf("%#v", e)
}

type Instrument struct {
	Date              string          `json:"date"`
	Name              string          `json:"name"`
	Iid               int64           `json:"iid"`
	AssetID           string          `json:"assetId"`
	CurrencyID        string          `json:"currencyId"`
	Lot               float64         `json:"lot"`
	Tick              float64         `json:"tick"`
	Classification    Classification  `json:"classification"`
	AvailableForTrade bool            `json:"availableForTrade"`
	AvailableForOTC   bool            `json:"availableForOTC"`
	Asset             GeneralAsset    `json:"asset"`
	Currency          GeneralCurrency `json:"currency"`
	Underlying        *string         `json:"underlying"`
}

type Classification string

const (
	ClassificationSpot          Classification = "S"
	ClassificationFuture        Classification = "F"
	ClassificationTest          Classification = "T"
	ClassificationPerpetualSwap Classification = "W"
)

type GeneralAsset struct {
	MinWithdrawalAmount float64 `json:"minWithdrawalAmount"`
	ID                  string  `json:"id"`
	Name                string  `json:"name"`
	MinIncrement        int64   `json:"minIncrement"`
	NetworkID           string  `json:"networkId"`
}

type GeneralCurrency struct {
	MinWithdrawalAmount int64  `json:"minWithdrawalAmount"`
	ID                  string `json:"id"`
	Name                string `json:"name"`
	MinIncrement        int64  `json:"minIncrement"`
	NetworkID           string `json:"networkId"`
}

type Venue struct {
	Code string `json:"code"`
	Date string `json:"date"`
	Vid  int64  `json:"vid"`
	Name string `json:"name"`
}
type Asset struct {
	ID                      string         `json:"id"`
	Name                    string         `json:"name"`
	MinWithdrawalAmount     float64        `json:"minWithdrawalAmount"`
	Rate                    float64        `json:"rate"`
	MinIncrement            float64        `json:"minIncrement"`
	Classification          Classification `json:"classification"`
	Logo                    string         `json:"logo"`
	AvailableProgramTrading bool           `json:"availableProgramTrading"`
	AvailableEarn           bool           `json:"availableEarn"`
}

type Network struct {
	ID          string      `json:"id"`
	Name        string      `json:"name"`
	ExplorerURL interface{} `json:"explorerUrl"`
	Assets      []Asset     `json:"assets"`
}

type Order struct {
	Mpid            string           `json:"mpid"`
	Qid             string           `json:"qid"`
	Token           string           `json:"token"`
	Iid             int64            `json:"iid"`
	Instrument      string           `json:"instrument"`
	Vid             int64            `json:"vid"`
	Venue           string           `json:"venue"`
	Price           float64          `json:"price"`
	Volume          float64          `json:"volume"`
	Time            time.Time        `json:"time"`
	Day             string           `json:"day"`
	Tif             OrderTimeInForce `json:"tif"`
	Type            OrderType        `json:"type"`
	Status          OrderStatus      `json:"status"`
	ExecutedVolume  float64          `json:"executedVolume"`
	Side            GeneralOrderSide `json:"side"`
	AveragePrice    float64          `json:"averagePrice"`
	ExecutionCount  int64            `json:"executionCount"`
	AssetID         string           `json:"assetId"`
	CurrencyID      string           `json:"currencyId"`
	Classification  Classification   `json:"classification"`
	RequestedVolume float64          `json:"requestedVolume"`
	RequestedPrice  float64          `json:"requestedPrice"`
	Total           float64          `json:"total"`
	// VirtualAccountID *int64           `json:"virtualAccountId,omitempty"`
}

type OrderTimeInForce string

const (
	OrderTimeInForceImmediateOrCancel OrderTimeInForce = "0"
	OrderTimeInForceGoodTilCanceled   OrderTimeInForce = "9"
	OrderTimeInForceDayOrder          OrderTimeInForce = "D"
)

type OrderStatus string

const (
	OrderStatusReceived OrderStatus = "received"
	OrderStatusOpen     OrderStatus = "open"
	OrderStatusClosed   OrderStatus = "closed"
)

type GeneralOrderSide string

const (
	GeneralOrderSideBuy  GeneralOrderSide = "B"
	GeneralOrderSideSell GeneralOrderSide = "S"
)

func (g GeneralOrderSide) String() string {
	return string(g)
}

type DMAOrderType string

const (
	DMAOrderTypeIoC      DMAOrderType = "ioc"
	DMAOrderTypeGTC      DMAOrderType = "gtc"
	DMAOrderTypeDayOrder DMAOrderType = "day"
	DMAOrderTypePOD      DMAOrderType = "pod"
)

func (t DMAOrderType) String() string {
	return string(t)
}

type AddDMAOrderRequest struct {
	Token            string    `json:"token"`
	Instrument       string    `json:"instrument"`
	Side             OrderSide `json:"side"`
	Volume           float64   `json:"volume"`
	Price            float64   `json:"price"`
	Venue            string    `json:"venue"`
	VirtualAccountID *int64    `json:"virtualAccountId,omitempty"`
}

type OrderSide string

const (
	OrderSideBuy        OrderSide = "buy"
	OrderSideSell       OrderSide = "sell"
	OrderSideOpenLong   OrderSide = "open-long"
	OrderSideCloseLong  OrderSide = "close-long"
	OrderSideOpenShort  OrderSide = "open-short"
	OrderSideCloseShort OrderSide = "close-short"
)

type SOROrderType string

const (
	SOROrderTypeISO SOROrderType = "iso"
	SOROrderTypeIDO SOROrderType = "ido"
	SOROrderTypeIGO SOROrderType = "igo"
	SOROrderTypeIPO SOROrderType = "ipo"
)

func (t SOROrderType) String() string {
	return string(t)
}

type AddSOROrderRequest struct {
	Token            string            `json:"token"`
	Instrument       string            `json:"instrument"`
	Side             OrderSide         `json:"side"`
	Volume           float64           `json:"volume"`
	Price            float64           `json:"price"`
	Priority         *SorOrderPriority `json:"priority"`
	VirtualAccountID *int64            `json:"virtualAccountId,omitempty"`
}

type SorOrderPriority string

const (
	SorOrderPriorityPrice SorOrderPriority = "price"
	SorOrderPriorityTime  SorOrderPriority = "time"
	SorOrderPriorityBoth  SorOrderPriority = "both"
)

type DSAOrderType string

const (
	DSAOrderTypeVIO  DSAOrderType = "vio"
	DSAOrderTypeTWAP DSAOrderType = "twap"
	DSAOrderTypeVWAP DSAOrderType = "vwap"
)

func (t DSAOrderType) String() string {
	return string(t)
}

type AddDSAOrderRequest struct {
	Token            string    `json:"token"`
	Instrument       string    `json:"instrument"`
	Side             OrderSide `json:"side"`
	Volume           float64   `json:"volume"`
	Price            float64   `json:"price"`
	Participation    *int64    `json:"participation,omitempty"`
	Lifespan         *int64    `json:"lifespan,omitempty"`
	VirtualAccountID *int64    `json:"virtualAccountId,omitempty"`
}

type TMOOrderType string

const (
	TMOOrderTypePTO TMOOrderType = "pto"
	TMOOrderTypeVTO TMOOrderType = "vto"
	TMOOrderTypeSIO TMOOrderType = "slo"
	TMOOrderTypeTPO TMOOrderType = "tpo"
)

func (t TMOOrderType) String() string {
	return string(t)
}

type TMOAnchor string

const (
	TMOAnchorMid   TMOAnchor = "mid"
	TMOAnchorBid   TMOAnchor = "bid"
	TMOAnchorOffer TMOAnchor = "offer"
	TMOAnchorLast  TMOAnchor = "last"
)

type AddTMOOrderRequest struct {
	Token            string     `json:"token"`
	Instrument       string     `json:"instrument"`
	Side             OrderSide  `json:"side"`
	Volume           float64    `json:"volume"`
	Price            float64    `json:"price"`
	Stop             *float64   `json:"stop,omitempty"`
	Achor            *TMOAnchor `json:"anchor,omitempty"`
	VirtualAccountID *int64     `json:"virtualAccountId,omitempty"`
}

type ModifyOrderRequest struct {
	Instrument string  `json:"instrument"`
	Volume     float64 `json:"volume"`
	Price      float64 `json:"price"`
}

type ReduceOrderRequest struct {
	Instrument string  `json:"instrument"`
	Volume     float64 `json:"volume"`
}

type GetOrdersRequest struct {
	Start      *int64            `json:"start"`
	End        *int64            `json:"end"`
	Instrument *string           `json:"instrument"`
	Side       *GeneralOrderSide `json:"side"`
	HideClosed *bool             `json:"hideClosed"`
	Order      *SortOrder        `json:"order"`
}

type GetOrdersResponse struct {
	Orders []Order `json:"data"`
	Count  int64   `json:"count"`
}

type GetExecutionsResponse struct {
	Executions []Execution `json:"data"`
	Count      int64       `json:"count"`
}

type Execution struct {
	Mpid       string           `json:"mpid"`
	Token      string           `json:"token"`
	Qid        string           `json:"qid"`
	Venue      string           `json:"venue"`
	Vid        int64            `json:"vid"`
	Instrument string           `json:"instrument"`
	Iid        int64            `json:"iid"`
	Side       GeneralOrderSide `json:"side"`
	Price      float64          `json:"price"`
	Volume     float64          `json:"volume"`
	Liquidity  Liquidity        `json:"liquidity"`
	Charge     int64            `json:"charge"`
	Rebate     int64            `json:"rebate"`
	Mid        string           `json:"mid"`
	Reason     Reason           `json:"reason"`
	Time       time.Time        `json:"time"`
}

type Liquidity string

const (
	LiquidityMaker Liquidity = "A" // "add" liquidity
	LiquidityTaker Liquidity = "R" // "remove" liquidity
)

type Reason string

const (
	ReasonExecution           Reason    = "E"
	ReasonSelfMatchPrevention Liquidity = "S"
)

type Balance struct {
	Quantity  float64 `json:"quantity"`
	Committed float64 `json:"committed"`
	AssetID   string  `json:"assetId"`
	Mpid      string  `json:"mpid"`
	Notional  float64 `json:"notional"`
	Asset     Asset   `json:"asset"`
}

type User struct {
	ID                   string                 `json:"id"`
	Email                string                 `json:"email"`
	Firstname            string                 `json:"firstname"`
	Lastname             string                 `json:"lastname"`
	TotpValidated        bool                   `json:"totpValidated"`
	TosUpdatedAt         string                 `json:"tosUpdatedAt"`
	LastSigninAt         string                 `json:"lastSigninAt"`
	DefaultAccountID     string                 `json:"defaultAccountId"`
	DefaultParticipantID string                 `json:"defaultParticipantId"`
	CancelDisconnect     bool                   `json:"cancelDisconnect"`
	MdfRefreshTime       int64                  `json:"mdfRefreshTime"`
	CreatedAt            string                 `json:"createdAt"`
	Participants         map[string]Participant `json:"participants"`
	Accounts             map[string]Account     `json:"accounts"`
}

type Participant struct {
	Mpid        string  `json:"mpid"`
	AccountID   string  `json:"accountId"`
	MaxRate     int64   `json:"maxRate"`
	MaxVol      int64   `json:"maxVol"`
	MaxDailyVol float64 `json:"maxDailyVol"`
	MaxOpen     int64   `json:"maxOpen"`
	CreatedAt   string  `json:"createdAt"`
}

type Account struct {
	ID          string      `json:"id"`
	Name        string      `json:"name"`
	Country     string      `json:"country"`
	Role        AccountRole `json:"role"`
	Permissions []string    `json:"permissions"`
}

type AccountRole struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	CreatedAt string `json:"createdAt"`
}

type DepositAddress struct {
	MinWithdrawalAmount float64   `json:"minWithdrawalAmount"`
	Rate                float64   `json:"rate"`
	ID                  string    `json:"id"`
	Name                string    `json:"name"`
	MinIncrement        float64   `json:"minIncrement"`
	Addresses           []Address `json:"addresses"`
}

type WithdrawalAddress struct {
	MinWithdrawalAmount float64     `json:"minWithdrawalAmount"`
	Rate                float64     `json:"rate"`
	ID                  string      `json:"id"`
	Name                string      `json:"name"`
	MinIncrement        float64     `json:"minIncrement"`
	Addresses           []Address   `json:"addresses"`
	Custodians          []Custodian `json:"custodians"`
}

type Custodian struct {
	ID               string    `json:"id"`
	Name             string    `json:"name"`
	CreatedAt        time.Time `json:"createdAt"`
	CustodianAccount string    `json:"custodianAccount"`
}

type AddressOwnerType string

const (
	AddressOwnerTypeAccount AddressOwnerType = "account"
)

type Address struct {
	ID               string           `json:"id"`
	Value            string           `json:"value"`
	Description      string           `json:"description"`
	OwnerID          string           `json:"ownerId"`
	OwnerType        AddressOwnerType `json:"ownerType"`
	IsInternal       bool             `json:"isInternal"`
	IsActive         bool             `json:"isActive"`
	IsWhitelisted    *bool            `json:"isWhitelisted"`
	IsArchived       bool             `json:"isArchived"`
	CreatedAt        *time.Time       `json:"createdAt"`
	NetworkID        *string          `json:"networkId"`
	VirtualAccountID *int64           `json:"virtualAccountId"`
}

type RequestWithdrawalRequest struct {
	AssetID     string  `json:"assetId"`
	Address     string  `json:"address"`
	Quantity    string  `json:"quantity"`
	Description string  `json:"description"`
	IncludeFees bool    `json:"includeFees"`
	Memo        *string `json:"memo,omitempty"`
	NetworkID   *string `json:"networkId,omitempty"`
}

type RequestWithdrawalResponse struct {
	ID                  string `json:"id"`
	AssetID             string `json:"assetId"`
	DestinationID       string `json:"destinationId"`
	DestinationType     string `json:"destinationType"`
	Quantity            string `json:"quantity"`
	Description         string `json:"description"`
	AccountID           string `json:"accountId"`
	Status              string `json:"status"`
	IncludeFees         bool   `json:"includeFees"`
	NetworkFeesQuantity string `json:"networkFeesQuantity"`
	ServiceFeesQuantity string `json:"serviceFeesQuantity"`
	NetworkFeesNotional string `json:"networkFeesNotional"`
	ServiceFeesNotional string `json:"serviceFeesNotional"`
}

type GetWithdrawalRequestsResponse struct {
	Data  []WithdrawalRequest `json:"data"`
	Count int64               `json:"count"`
}

type WithdrawalRequest struct {
	ID              string      `json:"id"`
	Quantity        string      `json:"quantity"`
	AccountID       string      `json:"accountId"`
	AssetID         string      `json:"assetId"`
	DestinationID   string      `json:"destinationId"`
	DestinationType string      `json:"destinationType"`
	PaymentID       interface{} `json:"paymentId"`
	Description     string      `json:"description"`
	Status          string      `json:"status"`
	CreatedAt       time.Time   `json:"createdAt"`
}

type GetDepositsHistoryResponse struct {
	Data  []Deposit `json:"data"`
	Count int       `json:"count"`
}

type Deposit struct {
	ID                 string    `json:"id"`
	AddressSrcID       string    `json:"addressSrcId"`
	AddressDestID      string    `json:"addressDestId"`
	Status             string    `json:"status"`
	TxID               string    `json:"txId"`
	Description        *string   `json:"description"`
	AssetID            string    `json:"assetId"`
	Quantity           float64   `json:"quantity"`
	FeesQuantity       float64   `json:"feesQuantity"`
	FeesAssetID        string    `json:"feesAssetId"`
	CreatedAt          time.Time `json:"createdAt"`
	SourceAddress      Address   `json:"sourceAddress"`
	DestinationAddress Address   `json:"destinationAddress"`
}

type GetWithdrawalsHistoryResponse struct {
	Data  []Withdrawal `json:"data"`
	Count int64        `json:"count"`
}

type Withdrawal struct {
	ID                 string           `json:"id"`
	AddressSrcID       string           `json:"addressSrcId"`
	AddressDestID      string           `json:"addressDestId"`
	Status             WithdrawalStatus `json:"status"`
	TxID               *string          `json:"txId"`
	Description        *string          `json:"description"`
	AssetID            string           `json:"assetId"`
	Quantity           string           `json:"quantity"`
	FeesQuantity       string           `json:"feesQuantity"`
	FeesAssetID        string           `json:"feesAssetId"`
	CreatedAt          time.Time        `json:"createdAt"`
	SourceAddress      Address          `json:"sourceAddress"`
	DestinationAddress Address          `json:"destinationAddress"`
}

type WithdrawalStatus string

const (
	WithdrawalStatusSent                 WithdrawalStatus = "sent"
	WithdrawalStatusError                WithdrawalStatus = "error"
	WithdrawalStatusProcessing           WithdrawalStatus = "processing"
	WithdrawalStatusPendingInvestigation WithdrawalStatus = "pendingInvestigation"
	WithdrawalStatusComplete             WithdrawalStatus = "complete"
	WithdrawalStatusFrozen               WithdrawalStatus = "frozen"
)

type RequestTransferRequest struct {
	AssetID       string  `json:"assetId"`
	SourceID      string  `json:"sourceId"`
	DestinationID string  `json:"destinationId"`
	Quantity      float64 `json:"quantity"`
	Description   *string `json:"description"`
}

type RequestTransferResponse struct {
	ID            string                `json:"id"`
	SourceID      string                `json:"sourceId"`
	DestinationID string                `json:"destinationId"`
	Description   *string               `json:"description"`
	UserID        string                `json:"userId"`
	AccountID     string                `json:"accountId"`
	AssetID       string                `json:"assetId"`
	Status        TransferRequestStatus `json:"status"`
	Quantity      float64               `json:"quantity"`
	CreatedAt     time.Time             `json:"createdAt"`
}

type TransferRequestStatus string

const (
	TransferRequestStatusRequested  TransferRequestStatus = "requested"
	TransferRequestStatusProcessing TransferRequestStatus = "processing"
	TransferRequestStatusApproved   TransferRequestStatus = "approved"
	TransferRequestStatusRejected   TransferRequestStatus = "rejected"
)

type TransferRequest struct {
	ID            string                `json:"id"`
	SourceID      string                `json:"sourceId"`
	DestinationID string                `json:"destinationId"`
	UserID        string                `json:"userId"`
	AccountID     string                `json:"accountId"`
	AssetID       string                `json:"assetId"`
	Status        TransferRequestStatus `json:"status"`
	Quantity      float64               `json:"quantity"`
	RebalancingID *string               `json:"rebalancingId"`
	CreatedAt     time.Time             `json:"createdAt"`
}

type GetTransfersHistoryResponse struct {
	Data  []TransferRequest `json:"data"`
	Count int64             `json:"count"`
}

type GetActivityRequest struct {
	Account    *string `json:"account"`
	SubAccount *string `json:"subAccount"`
	Asset      *string `json:"asset"`
	Start      *int64  `json:"start"`
	End        *int64  `json:"end"`
}

type GetActivityResponse struct {
	Data  []Activity `json:"data"`
	Count int        `json:"count"`
}

type Activity struct {
	Time           time.Time    `json:"time"`
	Type           ActivityType `json:"type"`
	Asset          string       `json:"asset"`
	Quantity       int64        `json:"quantity"`
	UpdatedBalance int64        `json:"updatedBalance"`
	SubAccount     string       `json:"subAccount"`
	ActivityID     string       `json:"activityId"`
	ActivityNote   string       `json:"activityNote"`
}

type ActivityType string

const (
	ActivityTypeDeposit    ActivityType = "deposit"
	ActivityTypeWithdrawal ActivityType = "withdrawal"
	ActivityTypeTransfer   ActivityType = "transfer"
	ActivityTypeTrade      ActivityType = "trade"
	ActivityTypeRebate     ActivityType = "rebate"
	ActivityTypeCharge     ActivityType = "charge"
	ActivityTypeConversion ActivityType = "conversion"
	ActivityTypeSettlement ActivityType = "settlement"
)

type GetBalancesRequest struct {
	Account *string `json:"account"`
	Asset   *string `json:"asset"`
	Date    *int64  `json:"date"`
}

type GetBalancesResponse struct {
	Date        time.Time              `json:"date"`
	Total       interface{}            `json:"total"`
	SubAccounts map[string]interface{} `json:"subAccounts"`
}

type GetOrdersHistoryRequest struct {
	Account    *string    `json:"account"`
	MPID       *string    `json:"mpid"`
	Start      *int64     `json:"start"`
	End        *int64     `json:"end"`
	HideClosed *bool      `json:"hideClosed"`
	Order      *SortOrder `json:"order"`
}

type SortOrder string

const (
	SortOrderAsc  SortOrder = "asc"
	SortOrderDesc SortOrder = "desc"
)

func (s SortOrder) String() string {
	return string(s)
}

type GetOrdersHistoryResponse struct {
	Data  []OrderHistory `json:"data"`
	Count int            `json:"count"`
}

type OrderHistory struct {
	Time            time.Time   `json:"time"`
	Day             string      `json:"day"`
	User            string      `json:"user"`
	Mpid            string      `json:"mpid"`
	OrderID         string      `json:"orderId"`
	Status          OrderStatus `json:"status"`
	Type            OrderType   `json:"type"`
	Side            OrderSide   `json:"side"`
	Asset           string      `json:"asset"`
	Currency        string      `json:"currency"`
	RequestedVolume string      `json:"requestedVolume"`
	RequestedPrice  string      `json:"requestedPrice"`
	ExecutedVolume  string      `json:"executedVolume"`
	AveragePrice    string      `json:"averagePrice"`
	Total           string      `json:"total"`
	ExecutionCount  string      `json:"executionCount"`
}

type OrderType string

const (
	OrderTypeDAY  OrderType = "DAY"
	OrderTypeGTC  OrderType = "GTC"
	OrderTypeIOC  OrderType = "IOC"
	OrderTypeIDO  OrderType = "IDO"
	OrderTypeIGO  OrderType = "IGO"
	OrderTypeISO  OrderType = "ISO"
	OrderTypeVIO  OrderType = "VIO"
	OrderTypeTWAP OrderType = "TWAP"
	OrderTypeVWAP OrderType = "VWAP"
	OrderTypeVTO  OrderType = "VTO"
	OrderTypeSLO  OrderType = "SLO"
	OrderTypeTPO  OrderType = "TPO"
	OrderTypeTEO  OrderType = "TEO"
)

type GetTradesHistoryRequest struct {
	Account *string `json:"account"`
	MPID    *string `json:"mpid"`
	Asset   *string `json:"asset"`
	Start   *int64  `json:"start"`
	End     *int64  `json:"end"`
}

type GetTradesHistoryResponse struct {
	Data  []Trade `json:"data"`
	Count int     `json:"count"`
}

type Trade struct {
	Time      time.Time      `json:"time"`
	Mpid      string         `json:"mpid"`
	OrderID   string         `json:"orderId"`
	Side      TradeSide      `json:"side"`
	Asset     string         `json:"asset"`
	Currency  string         `json:"currency"`
	Volume    string         `json:"volume"`
	Price     string         `json:"price"`
	Total     string         `json:"total"`
	Liquidity TradeLiquidity `json:"liquidity"`
	Venue     string         `json:"venue"`
}

type TradeSide string

const (
	TradeSideSell TradeSide = "Sell"
	TradeSideBuy  TradeSide = "Buy"
)

type TradeLiquidity string

const (
	TradeLiquidityMake TradeLiquidity = "Make"
	TradeLiquidityTake TradeLiquidity = "Take"
)

type AnalyticUniverse map[string]AnalyticRoute

type AnalyticRoute struct {
	Venue     []string  `json:"venue"`
	Geography *[]string `json:"geography"`
	DayOfWeek *[]string `json:"dayOfWeek"`
	Lookback  *[]string `json:"lookback"`
	Lookahead *[]string `json:"lookahead"`
}

type AnalyticParameters struct {
	Instrument string  `json:"instrument"`
	Venue      *string `json:"venue"`
	Geography  *string `json:"geography"`
	DayOfWeek  *string `json:"dayOfWeek"`
	Lookback   *string `json:"lookback"`
	Lookahead  *string `json:"lookahead"`
}

type GetTradesAverageSizeResponse struct {
	Instrument string  `json:"instrument"`
	Lookback   string  `json:"lookback"`
	Size       float64 `json:"size"`
	Venue      string  `json:"venue"`
	Geography  string  `json:"geography"`
	DayOfWeek  string  `json:"dayOfWeek"`
}

type GetDailyAverageVolumeResponse struct {
	Instrument string  `json:"instrument"`
	Lookback   string  `json:"lookback"`
	Volume     float64 `json:"volume"`
	Venue      string  `json:"venue"`
	Geography  string  `json:"geography"`
	DayOfWeek  string  `json:"dayOfWeek"`
}

type GetMoneyFlowResponse struct {
	Instrument string  `json:"instrument"`
	Lookback   string  `json:"lookback"`
	MoneyFlow  float64 `json:"moneyFlow"`
	Venue      string  `json:"venue"`
	Geography  string  `json:"geography"`
	DayOfWeek  string  `json:"dayOfWeek"`
}

type GetAnualizedVolatilityResponse struct {
	Instrument string  `json:"instrument"`
	Lookback   string  `json:"lookback"`
	Volatility float64 `json:"volatility"`
	Venue      string  `json:"venue"`
	Geography  string  `json:"geography"`
	DayOfWeek  string  `json:"dayOfWeek"`
}

type GetEstimatedSeasonalityResponse struct {
	Instrument  string  `json:"instrument"`
	Lookahead   string  `json:"lookahead"`
	Seasonality float64 `json:"seasonality"`
	Venue       string  `json:"venue"`
}

type GetMarketImpactRequest struct {
	Instrument string   `json:"instrument"`
	Impact     *float64 `json:"impact"`
	Volume     *float64 `json:"volume"`
	Lookahead  *string  `json:"lookahead"`
}

type GetMarketImpactResponse struct {
	Instrument string  `json:"instrument"`
	Lookahead  string  `json:"lookahead"`
	Volume     float64 `json:"volume"`
	Impact     float64 `json:"impact"`
	Venue      string  `json:"venue"`
}

type GetAllVirtualAccountsResponse struct {
	Data  []VirtualAccount `json:"data"`
	Count int64            `json:"count"`
}

type VirtualAccount struct {
	ID        int64   `json:"id"`
	AccountID string  `json:"accountId"`
	Name      *string `json:"name,omitempty"`
	Email     string  `json:"email"`
	CreatedAt string  `json:"createdAt"`
}

type AddWithdrawalAddressRequest struct {
	Value       string   `json:"value"`
	Description string   `json:"description"`
	NetworkID   string   `json:"networkId"`
	Memo        *string  `json:"memo,omitempty"`
	Asset       []string `json:"asset"`
}

type GetAllAddressesResponse struct {
	Data  []VirtualAccountAddress `json:"data"`
	Count int64                   `json:"count"`
}

type VirtualAccountAddress struct {
	ID               string   `json:"id"`
	NetworkID        string   `json:"networkId"`
	Value            string   `json:"value"`
	Description      string   `json:"description"`
	Memo             *string  `json:"memo"`
	OwnerType        string   `json:"ownerType"`
	OwnerID          string   `json:"ownerId"`
	IsActive         bool     `json:"isActive"`
	IsArchived       bool     `json:"isArchived"`
	IsInternal       bool     `json:"isInternal"`
	IsWhitelisted    bool     `json:"isWhitelisted"`
	VirtualAccountID int64    `json:"virtualAccountId"`
	CreatedAt        string   `json:"createdAt"`
	Assets           []string `json:"assets"`
}

type RequestTransferFromVirtualAccountRequest struct {
	AssetID     string  `json:"assetId"`
	Source      string  `json:"source"`
	Destination string  `json:"destination"`
	Quantity    float64 `json:"quantity"`
	Description *string `json:"description"`
}

type TransferRequestFromVirtualAccount struct {
	ID               string                `json:"id"`
	UserID           string                `json:"userId"`
	AssetID          string                `json:"assetId"`
	Description      *string               `json:"description"`
	Quantity         float64               `json:"quantity"`
	AccountID        string                `json:"accountId"`
	Status           TransferRequestStatus `json:"status"`
	Source           string                `json:"source"`
	Destination      string                `json:"destination"`
	VirtualAccountID int64                 `json:"virtualAccountId"`
}

type GetTransferRequestsResponse struct {
	Data  []TransferRequestFromVirtualAccount `json:"data"`
	Count int64                               `json:"count"`
}

type RequestWithdrawalFromVirtualAccountRequest struct {
	AssetID     string  `json:"assetId"`
	AddressID   string  `json:"addressID"`
	Quantity    float64 `json:"quantity"`
	Description *string `json:"description"`
}

type WithdrawalRequestFromVirtualAccount struct {
	ID                  string        `json:"id"`
	UserID              string        `json:"userId"`
	AssetID             string        `json:"assetId"`
	Description         *string       `json:"description"`
	Quantity            float64       `json:"quantity"`
	AccountID           string        `json:"accountId"`
	NetworkFeesQuantity float64       `json:"networkFeesQuantity"`
	ServiceFeesQuantity float64       `json:"serviceFeesQuantity"`
	NetworkFeesNotional float64       `json:"networkFeesNotional"`
	ServiceFeesNotional float64       `json:"serviceFeesNotional"`
	PaymentID           interface{}   `json:"paymentId"`
	DestinationID       string        `json:"destinationId"`
	DestinationType     string        `json:"destinationType"`
	Status              string        `json:"status"`
	IncludeFees         bool          `json:"includeFees"`
	SelectedPolicy      interface{}   `json:"selectedPolicy"`
	ValidatedBy         []interface{} `json:"validatedBy"`
	VirtualAccountID    int64         `json:"virtualAccountId"`
	CreatedAt           time.Time     `json:"createdAt"`
}

type VirtualAccountWithdrawalStatus string

const (
	VirtualAccountWithdrawalStatusRequested   VirtualAccountWithdrawalStatus = "requested"
	VirtualAccountWithdrawalStatusApproved    VirtualAccountWithdrawalStatus = "approved"
	VirtualAccountWithdrawalStatusRejectedSys VirtualAccountWithdrawalStatus = "rejectedSys"
)

func (s VirtualAccountWithdrawalStatus) String() string {
	return string(s)
}

type GetVirtualAccountWithdrawalRequestsResponse struct {
	Data  []WithdrawalRequestFromVirtualAccount `json:"data"`
	Count int64                                 `json:"count"`
}

type GetVirtualAccountOrderRequest struct {
	VirtualAccountID string `json:"virtualAccountId"`
	MPID             string `json:"mpid"`
	Token            string `json:"token"`
	Day              string `json:"day"`
}

type GetAllVirtualAccountsBalancesRequest struct {
	SubAccount *string `json:"subAccount"`
	Date       *int64  `json:"date"`
}

type GetAllVirtualAccountsBalancesResponse struct {
	Date     string                        `json:"date"`
	Balances map[string]map[string]float64 `json:"balances"`
}

type GetVirtualAccountBalancesRequest struct {
	Date *int64 `json:"date"`
}

type GetVirtualAccountBalancesResponse struct {
	Date        string                        `json:"date"`
	Total       map[string]float64            `json:"total"`
	SubAccounts map[string]map[string]float64 `json:"subAccounts"`
}

type GetVirtualAccountActivityRequest struct {
	Start      *int64                      `json:"start"`
	End        *int64                      `json:"end"`
	SubAccount *string                     `json:"subAccount"`
	Type       *VirtualAccountActivityType `json:"type"`
	AssetID    *string                     `json:"assetId"`
	Order      *SortOrder                  `json:"order"`
}

type VirtualAccountActivityType string

const (
	VirtualAccountActivityTypeWithdrawal    VirtualAccountActivityType = "withdrawal"
	VirtualAccountActivityTypeDeposit       VirtualAccountActivityType = "deposit"
	VirtualAccountActivityTypeTransfer      VirtualAccountActivityType = "transfer"
	VirtualAccountActivityTypeTrade         VirtualAccountActivityType = "trade"
	VirtualAccountActivityTypeCharge        VirtualAccountActivityType = "charge"
	VirtualAccountActivityTypeRebate        VirtualAccountActivityType = "rebate"
	VirtualAccountActivityTypeSettlement    VirtualAccountActivityType = "settlement"
	VirtualAccountActivityTypeConversion    VirtualAccountActivityType = "conversion"
	VirtualAccountActivityTypeEarnInterests VirtualAccountActivityType = "earnInterests"
	VirtualAccountActivityTypeRebalancing   VirtualAccountActivityType = "rebalancing"
	VirtualAccountActivityTypeFunding       VirtualAccountActivityType = "funding"
)

func (t VirtualAccountActivityType) String() string {
	return string(t)
}

type VirtualAccountActivity struct {
	Time             string `json:"time"`
	VirtualAccountID int64  `json:"virtualAccountId"`
	Type             string `json:"type"`
	Asset            string `json:"asset"`
	Quantity         int64  `json:"quantity"`
	SubAccount       string `json:"subAccount"`
	UpdatedBalance   int64  `json:"updatedBalance"`
	ActivityID       string `json:"activityId"`
	ActivityNote     string `json:"activityNote"`
}

type GetVirtualAccountsActivityResponse struct {
	Data  []VirtualAccountActivity `json:"data"`
	Count int64                    `json:"count"`
}

type GetVirtualAccountsTradesRequest struct {
	Start *int64     `json:"start"`
	End   *int64     `json:"end"`
	MPID  *string    `json:"mpid"`
	Order *SortOrder `json:"order"`
}

type VirtualAccountTrade struct {
	Time             string  `json:"time"`
	Mpid             string  `json:"mpid"`
	OrderID          string  `json:"orderId"`
	Side             string  `json:"side"`
	Asset            string  `json:"asset"`
	Currency         string  `json:"currency"`
	Volume           float64 `json:"volume"`
	Price            float64 `json:"price"`
	Total            float64 `json:"total"`
	Liquidity        string  `json:"liquidity"`
	VirtualAccountID int64   `json:"virtualAccountId"`
}

type GetVirtualAccountsTradesResponse struct {
	Data  []VirtualAccountTrade `json:"data"`
	Count int64                 `json:"count"`
}

type GetAllWithdrawalsRequest struct {
	Start   *int64     `json:"start"`
	End     *int64     `json:"end"`
	AssetID *string    `json:"assetId"`
	Order   *SortOrder `json:"order"`
}

type GetAllDepositsRequest struct {
	Start   *int64     `json:"start"`
	End     *int64     `json:"end"`
	AssetID *string    `json:"assetId"`
	Order   *SortOrder `json:"order"`
}

type EarningAccountMovementRequest struct {
	AssetID  string  `json:"assetId"`
	Quantity float64 `json:"quantity"`
}

type EarningAccountMovement struct {
	ID               string                       `json:"id"`
	Quantity         float64                      `json:"quantity"`
	AssetID          string                       `json:"assetId"`
	AccountID        string                       `json:"accountId"`
	Type             string                       `json:"type"`
	Status           EarningAccountMovementStatus `json:"status"`
	Day              string                       `json:"day"`
	RequestedBy      string                       `json:"requestedBy"`
	VirtualAccountID int64                        `json:"virtualAccountId"`
	TxID             *string                      `json:"txId"`
	FundsDestination *string                      `json:"fundsDestination"`
	CreatedAt        string                       `json:"createdAt"`
}

type EarningAccountMovementStatus string

const (
	EarningAccountMovementStatusPending   EarningAccountMovementStatus = "pending"
	EarningAccountMovementStatusRejected  EarningAccountMovementStatus = "rejected"
	EarningAccountMovementStatusAccepted  EarningAccountMovementStatus = "accepted"
	EarningAccountMovementStatusProcessed EarningAccountMovementStatus = "processed"
)

func (e EarningAccountMovementStatus) String() string {
	return string(e)
}

type EarnRequestType string

const (
	EarnRequestTypeIncoming EarnRequestType = "incomingEarn"
	EarnRequestTypeOutgoing EarnRequestType = "outgoingEarn"
)

type GetVirtualAccountsEarnRequests struct {
	Status *EarningAccountMovementStatus `json:"status"`
	Type   *EarnRequestType              `json:"type"`
}

func (t EarnRequestType) String() string {
	return string(t)
}

type GetVirtualAccountsEarnResponse struct {
	Data  []EarningAccountMovement `json:"data"`
	Count int64                    `json:"count"`
}

type VirtualAccountsEarningInterests map[string]float64
type GetAllVirtualAccountsEarningInterestsResponse map[string]map[string]float64
