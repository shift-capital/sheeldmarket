package sheeldmarket

import (
	"fmt"
	"net/http"
)

//GetAvailableInstruments This route allows market participants to retrieve all instruments available for trading, alongside their name, ID and microstructures (trading rules).
//see: https://docs.trade.sheeldmatch.com/#get-available-instruments
func (s *SheeldMarket) GetAvailableInstruments() ([]*Instrument, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"instruments", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	resp := make([]*Instrument, 0)
	err = s.do(req, &resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAvailableVenues This route allows market participants to retrieve the venues available for trading.
//see: https://docs.trade.sheeldmatch.com/#get-available-venues
func (s *SheeldMarket) GetAvailableVenues() ([]Venue, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"venues", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	resp := make([]Venue, 0)
	err = s.do(req, &resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAvailableAssets This route allows market participants to retrieve the assets available in the sheeldmarket universe.
//see: https://docs.trade.sheeldmatch.com/#get-available-assets
func (s *SheeldMarket) GetAvailableAssets() ([]Asset, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"assets", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	resp := make([]Asset, 0)
	err = s.do(req, &resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAvailableNetworks This route allows market participants to retrieve the networks used in the sheeldmarket universe.
//see: https://docs.trade.sheeldmatch.com/#get-available-networks
func (s *SheeldMarket) GetAvailableNetworks() ([]Network, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"networks", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	resp := make([]Network, 0)
	err = s.do(req, &resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}
