package sheeldmarket

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/gorilla/websocket"
)

func ping(conn *websocket.Conn) (err error) {
	ticker := time.NewTicker(15 * time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			if err = conn.WriteMessage(websocket.PingMessage, []byte(`{"op": "pong"}`)); err != nil {
				return err
			}
		}
	}
}

func (s *SheeldMarket) Connect(ctx context.Context, ch chan MarketDataStream, params MarketDataWSPathParams) (*websocket.Conn, error) {
	urlExtension := params.ToString()
	url := s.wsMarketDataURL + urlExtension
	conn, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		return nil, err
	}

	// ping each 15sec for exchange
	go ping(conn)

	go func() {
		defer conn.Close()

	RESTART:
		for {
			var res MarketDataStream

			// Receive cancellation notification from the parent and execute CLOSE(unsubscribe->conn.Close). In this case RECONNECT is performed by the parent.
			select {
			case <-ctx.Done():
				fmt.Println("[INFO]: ws context done: %w", err)
				return

			default:
			}

			_, msg, err := conn.ReadMessage()
			if err != nil {
				fmt.Println("[ERROR]: msg error: %w", err)
				res.Type = ERROR
				res.Results = fmt.Errorf("%v\n", err)
				ch <- res
				break RESTART
			}

			msgPayload := MarketDataWSMessage{}
			err = json.Unmarshal(msg, &msgPayload)
			if err != nil {
				fmt.Printf("Unmarshal: %+v", err)
				res.Type = ERROR
				res.Results = fmt.Errorf("%v\n", err)
				break RESTART
			}

			switch msgPayload.Type {
			case MarketDataWSMessageTypeH, MarketDataWSMessageTypeL:
				// trades
				trades, err := parseTradeData(msgPayload)
				if err != nil {
					res.Type = ERROR
					res.Results = fmt.Errorf("%v\n", err)
					break RESTART
				}
				if trades != nil {
					res.Type = TRADES
					res.Trades = *trades
				}
			case MarketDataWSMessageTypeB, MarketDataWSMessageTypeS:
				// order books
				orderBooks, err := parseOrderBookData(msgPayload)
				if err != nil {
					res.Type = ERROR
					res.Results = fmt.Errorf("%v\n", err)
					break RESTART
				}
				if orderBooks != nil {
					res.Type = ORDERBOOK
					res.OrderBooks = *orderBooks
				}
			case MarketDataWSMessageTypeF:
				// funding rate
				fundingRate, err := parseFundingRateData(msgPayload)
				if err != nil {
					res.Type = ERROR
					res.Results = fmt.Errorf("%v\n", err)
					break RESTART
				}
				if fundingRate != nil {
					res.Type = FUNDINGRATE
					res.FundingRate = *fundingRate
				}
			default:
				res.Type = UNDEFINED
				res.Results = fmt.Errorf("%v", string(msg))
			}
			ch <- res

		}
	}()

	return conn, nil
}

func parseTradeData(msgPayload MarketDataWSMessage) (*MarketDataWSTrades, error) {
	dto, _ := json.Marshal(msgPayload.Data)
	marketDataWSTradeData := []MarketDataWSTradeData{}
	err := json.Unmarshal(dto, &marketDataWSTradeData)
	if err != nil {
		return nil, err
	}

	trades := MarketDataWSTrades{
		Type:       msgPayload.Type,
		Instrument: msgPayload.Instrument,
		Venue:      msgPayload.Venue,
		Data:       marketDataWSTradeData,
	}
	return &trades, nil
}

func parseOrderBookData(msgPayload MarketDataWSMessage) (*MarketDataWSOrderBooks, error) {
	dto, _ := json.Marshal(msgPayload.Data)
	marketDataWSOrderBookData := []MarketDataWSOrderBookData{}
	err := json.Unmarshal(dto, &marketDataWSOrderBookData)
	if err != nil {
		return nil, err
	}

	orderBook := MarketDataWSOrderBooks{
		Type:       msgPayload.Type,
		Instrument: msgPayload.Instrument,
		Venue:      msgPayload.Venue,
		Data:       marketDataWSOrderBookData,
	}
	return &orderBook, nil
}

func parseFundingRateData(msgPayload MarketDataWSMessage) (*MarketDataWSFundingRate, error) {
	dto, _ := json.Marshal(msgPayload.Data)
	marketDataWSFundingRateData := []MarketDataWSFundingRateData{}
	err := json.Unmarshal(dto, &marketDataWSFundingRateData)
	if err != nil {
		return nil, err
	}

	fundingRate := MarketDataWSFundingRate{
		Type:       msgPayload.Type,
		Instrument: msgPayload.Instrument,
		Venue:      msgPayload.Venue,
		Data:       marketDataWSFundingRateData,
	}
	return &fundingRate, nil
}
