package sheeldmarket

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

//GetAllVirtualAccounts This route returns all virtual accounts for the specified account.
//see: https://docs.trade.sheeldmatch.com/#get-all-virtual-accounts
func (s *SheeldMarket) GetAllVirtualAccounts(accountID string, email *string) (*GetAllVirtualAccountsResponse, error) {
	path := s.baseUrl + "accounts/" + accountID + "/virtual-accounts"
	u, _ := url.Parse(path)
	q := u.Query()
	if email != nil {
		q.Set("email", *email)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetAllVirtualAccountsResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountInformation This route allows users to retrieve the information of a specific virtual account.
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-information
func (s *SheeldMarket) GetVirtualAccountInformation(accountID, virtualAccountID string) (*VirtualAccount, error) {
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"accounts/"+accountID+"/virtual-accounts/"+virtualAccountID, nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(VirtualAccount)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//AddWithdrawalAddress This route allows you to add a withdrawal address for a specific virtual account
//see: https://docs.trade.sheeldmatch.com/#add-a-withdrawal-address
func (s *SheeldMarket) AddWithdrawalAddress(accountID, virtualAccountID string, val AddWithdrawalAddressRequest) error {
	body, err := json.Marshal(val)
	if err != nil {
		return fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"accounts/"+accountID+"/virtual-accounts/"+virtualAccountID+"/addresses/withdrawal", bytes.NewBuffer(body))
	if err != nil {
		return fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return fmt.Errorf("authenticate: %w", err)
	}
	err = s.do(req, nil)
	if err != nil {
		return fmt.Errorf("do: %w", err)
	}
	return nil
}

//GetAllWithdrawalAddresses This route allows you to get withdrawal addresses for ALL virtual accounts owned by the specified account.
//see: https://docs.trade.sheeldmatch.com/#get-all-withdrawal-addresses
func (s *SheeldMarket) GetAllWithdrawalAddresses(accountID string, assetID *string) (*GetAllAddressesResponse, error) {
	path := s.baseUrl + "accounts/" + accountID + "/virtual-accounts/addresses/withdrawal"
	u, _ := url.Parse(path)
	q := u.Query()
	if assetID != nil {
		q.Set("assetId", *assetID)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetAllAddressesResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetSpecificVirtualAccountWithdrawalAddresses This route allows you to get the withdrawal addresses for the specified virtual account
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-withdrawal-addresses
func (s *SheeldMarket) GetSpecificVirtualAccountWithdrawalAddresses(accountID, virtualAccountID string, assetID *string) (*GetAllAddressesResponse, error) {
	path := s.baseUrl + "accounts/" + accountID + "/virtual-accounts/" + virtualAccountID + "/addresses/withdrawal"
	u, _ := url.Parse(path)
	q := u.Query()
	if assetID != nil {
		q.Set("assetId", *assetID)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetAllAddressesResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAllDepositAddresses This route allows you to get deposit addresses for ALL virtual accounts owned by the specified account.
//see: https://docs.trade.sheeldmatch.com/#get-all-deposit-addresses
func (s *SheeldMarket) GetAllDepositAddresses(accountID string, assetID *string) (*GetAllAddressesResponse, error) {
	path := s.baseUrl + "accounts/" + accountID + "/virtual-accounts/addresses/deposit"
	u, _ := url.Parse(path)
	q := u.Query()
	if assetID != nil {
		q.Set("assetId", *assetID)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetAllAddressesResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetSpecificVirtualAccountDepositAddresses This route allows you to get the deposit addresses for the specified virtual account
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-deposit-addresses
func (s *SheeldMarket) GetSpecificVirtualAccountDepositAddresses(accountID, virtualAccountID string, assetID *string) (*GetAllAddressesResponse, error) {
	path := s.baseUrl + "accounts/" + accountID + "/virtual-accounts/" + virtualAccountID + "/addresses/deposit"
	u, _ := url.Parse(path)
	q := u.Query()
	if assetID != nil {
		q.Set("assetId", *assetID)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetAllAddressesResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//RequestTransferFromVirtualAccount This route allows you to request a transfer between market participants and your funding account.

// In order to trade or withdraw funds from a virtual acccount, said funds need to be in the correct sub account. Virtual accounts have 2 types of sub account:

// the funding sub account: this is where the funds from deposits are automatically assigned and from where a virtual account can withdraw any asset.
// any mpid (Market Participant Id): funds assigned to an MPID are available for trading, see the /user route for full list of your MPIDs.
// This route is used to move funds back and from the funding sub account depending on the need to trade or withdraw them.
//see: https://docs.trade.sheeldmatch.com/#request-a-transfer-from-a-virtual-account
func (s *SheeldMarket) RequestTransferFromVirtualAccount(accountID, virtualAccountID string, val RequestTransferFromVirtualAccountRequest) (*TransferRequestFromVirtualAccount, error) {
	body, err := json.Marshal(val)
	if err != nil {
		return nil, fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"accounts/"+accountID+"/virtual-accounts/"+virtualAccountID+"/transfer-requests", bytes.NewBuffer(body))
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(TransferRequestFromVirtualAccount)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAllTransferRequests This route allows you to get transfer request for ALL virtual accounts owned by the specified account.
//see: https://docs.trade.sheeldmatch.com/#get-all-transfer-requests
func (s *SheeldMarket) GetAllTransferRequests(accountID string, assetID *string) (*GetTransferRequestsResponse, error) {
	path := s.baseUrl + "accounts/" + accountID + "/virtual-accounts/transfer-requests"
	u, _ := url.Parse(path)
	q := u.Query()
	if assetID != nil {
		q.Set("assetId", *assetID)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetTransferRequestsResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountTransferRequests This route allows you to get the transfer request for the specified virtual account.
//see: https://docs.trade.sheeldmatch.com/#get-virtual-account-transfer-requests
func (s *SheeldMarket) GetVirtualAccountTransferRequests(accountID, virtualAccountID string, assetID *string) (*GetTransferRequestsResponse, error) {
	path := s.baseUrl + "accounts/" + accountID + "/virtual-accounts/" + virtualAccountID + "/transfer-requests"
	u, _ := url.Parse(path)
	q := u.Query()
	if assetID != nil {
		q.Set("assetId", *assetID)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetTransferRequestsResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetTransferRequest This route allows users to retrieve the information of a specific transfer request.
//see: https://docs.trade.sheeldmatch.com/#get-specific-transfer-request
func (s *SheeldMarket) GetTransferRequest(accountID, virtualAccountID, transferRequestID string) (*TransferRequest, error) {
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"accounts/"+accountID+"/virtual-accounts/"+virtualAccountID+"/transfer-requests/"+transferRequestID, nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(TransferRequest)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//RequestWithdrawalFromVirtualAccount This route allows you to request a withdrawal from the virtual account funding sub account. Only funds located on the funding sub account can be withdrawn. Look at the /transfer-request route in order to move funds back to the funding sub account before a withdrawal.
//see: https://docs.trade.sheeldmatch.com/#request-a-withdrawal-from-a-virtual-account
func (s *SheeldMarket) RequestWithdrawalFromVirtualAccount(accountID, virtualAccountID string, val RequestWithdrawalFromVirtualAccountRequest) (*WithdrawalRequestFromVirtualAccount, error) {
	body, err := json.Marshal(val)
	if err != nil {
		return nil, fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"accounts/"+accountID+"/virtual-accounts/"+virtualAccountID+"/withdrawal-requests", bytes.NewBuffer(body))
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(WithdrawalRequestFromVirtualAccount)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAllWithdrawalRequests This route allows you to get withdrawal request for ALL virtual accounts owned by the specified account.
//see: https://docs.trade.sheeldmatch.com/#get-all-withdrawal-requests
func (s *SheeldMarket) GetAllWithdrawalRequests(accountID string, status *VirtualAccountWithdrawalStatus, assetID *string) (*GetWithdrawalRequestsResponse, error) {
	path := s.baseUrl + "accounts/" + accountID + "/virtual-accounts/withdrawal-requests"
	u, _ := url.Parse(path)
	q := u.Query()
	if assetID != nil {
		q.Set("assetId", *assetID)
	}
	if status != nil {
		q.Set("assetId", status.String())
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetWithdrawalRequestsResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountWithdrawalRequests This route allows you to get the withdrawal requests for the specified virtual account.
//see: https://docs.trade.sheeldmatch.com/#get-virtual-account-withdrawal-requests
func (s *SheeldMarket) GetVirtualAccountWithdrawalRequests(accountID, virtualAccountID string, status *VirtualAccountWithdrawalStatus, assetID *string) (*GetWithdrawalRequestsResponse, error) {
	path := s.baseUrl + "accounts/" + accountID + "/virtual-accounts/" + virtualAccountID + "/withdrawal-requests"
	u, _ := url.Parse(path)
	q := u.Query()
	if assetID != nil {
		q.Set("assetId", *assetID)
	}
	if status != nil {
		q.Set("assetId", status.String())
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetWithdrawalRequestsResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountWithdrawalRequests This route allows users to retrieve the information of a specific withdrawal request
//see: https://docs.trade.sheeldmatch.com/#get-specific-withdrawal-request
func (s *SheeldMarket) GetWithdrawalRequest(accountID, virtualAccountID, withdrawalRequestID string) (*WithdrawalRequest, error) {
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"accounts/"+accountID+"/virtual-accounts/"+virtualAccountID+"/withdrawal-requests/"+withdrawalRequestID, nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(WithdrawalRequest)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetOrders This method allows market participants to retrieve all orders for a Market Participant.
//see: https://docs.trade.sheeldmatch.com/#get-orders
func (s *SheeldMarket) GetAllOrders(accountId string, val GetOrdersRequest) (*GetOrdersResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/orders"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.HideClosed != nil {
		q.Set("hideClosed", fmt.Sprintf("%t", *val.HideClosed))
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()

	// TODO - pagination
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetOrdersResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountOrders This route allow the user to get all orders passed by the specified virtual account
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-orders
func (s *SheeldMarket) GetVirtualAccountOrders(accountId, virtualAccountId string, val GetOrdersRequest) (*GetOrdersResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/" + virtualAccountId + "/orders"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.HideClosed != nil {
		q.Set("hideClosed", fmt.Sprintf("%t", *val.HideClosed))
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()

	// TODO - pagination
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetOrdersResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountOrder This route allow the user to get a specific order passed by the specified virtual account.
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-order
func (s *SheeldMarket) GetVirtualAccountOrder(accountId string, val GetVirtualAccountOrderRequest) (*Order, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"accounts/"+accountId+"/virtual-accounts/"+val.VirtualAccountID+"/orders/"+"-"+val.MPID+val.Token+"-"+val.Day, nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(Order)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountOrder This route allow the user to get a specific order passed by the specified virtual account.
//see: https://docs.trade.sheeldmatch.com/#get-all-virtual-accounts-balances
func (s *SheeldMarket) GetAllVirtualAccountsBalances(accountId string, val GetAllVirtualAccountsBalancesRequest) (*GetAllVirtualAccountsBalancesResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/balances"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.SubAccount != nil {
		q.Set("subAccount", *val.SubAccount)
	}
	if val.Date != nil {
		q.Set("date", fmt.Sprintf("%d", *val.Date))
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetAllVirtualAccountsBalancesResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountOrder This route allow the user to get a specific order passed by the specified virtual account.
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-balances
func (s *SheeldMarket) GetVirtualAccountBalances(accountId, virtualAccountID string, val GetVirtualAccountBalancesRequest) (*GetVirtualAccountBalancesResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/" + virtualAccountID + "/balances"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Date != nil {
		q.Set("date", fmt.Sprintf("%d", *val.Date))
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetVirtualAccountBalancesResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAllVirtualAccountsActivity This route allow the user to get the activity of ALL virtual accounts owned by the specified account.
//see: https://docs.trade.sheeldmatch.com/#get-all-virtual-accounts-activity
func (s *SheeldMarket) GetAllVirtualAccountsActivity(accountId string, val GetVirtualAccountActivityRequest) (*GetVirtualAccountsActivityResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/activity"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.SubAccount != nil {
		q.Set("subAccount", *val.SubAccount)
	}
	if val.Type != nil {
		q.Set("type", val.Type.String())
	}
	if val.AssetID != nil {
		q.Set("assetId", *val.AssetID)
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetVirtualAccountsActivityResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountActivity This route allow the user to get the activity for a specific virtual account
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-orders-2
func (s *SheeldMarket) GetVirtualAccountActivity(accountId, virtualAccountId string, val GetVirtualAccountActivityRequest) (*GetVirtualAccountsActivityResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/" + virtualAccountId + "/activity"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.SubAccount != nil {
		q.Set("subAccount", *val.SubAccount)
	}
	if val.Type != nil {
		q.Set("type", val.Type.String())
	}
	if val.AssetID != nil {
		q.Set("assetId", *val.AssetID)
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetVirtualAccountsActivityResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAllVirtualAccountsTrades This route allow the user to get the trade history of ALL virtual accounts owned by the specified account.
//see: https://docs.trade.sheeldmatch.com/#get-all-virtual-accounts-trades
func (s *SheeldMarket) GetAllVirtualAccountsTrades(accountId string, val GetVirtualAccountsTradesRequest) (*GetVirtualAccountsTradesResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/trades"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.MPID != nil {
		q.Set("mpid", *val.MPID)
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetVirtualAccountsTradesResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountTrades This route allow the user to get the trade history for a specific virtual account.
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-trades
func (s *SheeldMarket) GetVirtualAccountTrades(accountId, virtualAccountId string, val GetVirtualAccountsTradesRequest) (*GetVirtualAccountsTradesResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/" + virtualAccountId + "/trades"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.MPID != nil {
		q.Set("mpid", *val.MPID)
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetVirtualAccountsTradesResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAllWithdrawals This route allows you to get withdrawals for ALL virtual accounts owned by the specified account.
// The difference with the /withdrawal-requests route, is that this one only list approved withdrawal with the additional payment information
//see: https://docs.trade.sheeldmatch.com/#get-all-virtual-accounts-trades
func (s *SheeldMarket) GetAllWithdrawals(accountId string, val GetAllWithdrawalsRequest) (*GetWithdrawalRequestsResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/withdrawals"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.AssetID != nil {
		q.Set("assetId", *val.AssetID)
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetWithdrawalRequestsResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountWithdrawals This route allows you to get withdrawals for a specific virtual account. The difference with the /withdrawal-requests route, is that this one only list approved withdrawal with the additional payment information
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-withdrawals
func (s *SheeldMarket) GetVirtualAccountWithdrawals(accountId, virtualAccountId string, val GetAllWithdrawalsRequest) (*GetWithdrawalRequestsResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/" + virtualAccountId + "/withdrawals"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.AssetID != nil {
		q.Set("assetId", *val.AssetID)
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetWithdrawalRequestsResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAllDeposits This route allows you to get deposits for ALL virtual accounts owned by the specified account.
//see: https://docs.trade.sheeldmatch.com/#get-all-deposits
func (s *SheeldMarket) GetAllDeposits(accountId string, val GetAllDepositsRequest) (*GetDepositsHistoryResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/deposits"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.AssetID != nil {
		q.Set("assetId", *val.AssetID)
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetDepositsHistoryResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountDeposits This route allows you to get deposits for a specific virtual account.
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-deposits
func (s *SheeldMarket) GetVirtualAccountDeposits(accountId, virtualAccountId string, val GetAllDepositsRequest) (*GetDepositsHistoryResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/" + virtualAccountId + "/deposits"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.AssetID != nil {
		q.Set("assetId", *val.AssetID)
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetDepositsHistoryResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//DepositFundsToEarningAccount Our Earn product uses different sub accounts to manage the funds. The funds you want to earn with must be present on your "funding" sub account (use the /balances route).
// This route will automatically move the requested funds from the "funding" sub account to the "incomingEarn" sub account.
// Our backoffice will then process your request and your funds will then be located on the "earning" sub account.
//see: https://docs.trade.sheeldmatch.com/#deposit-funds-to-earning-account
func (s *SheeldMarket) DepositFundsToEarningAccount(accountID, virtualAccountID string, val EarningAccountMovementRequest) (*EarningAccountMovement, error) {
	body, err := json.Marshal(val)
	if err != nil {
		return nil, fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"accounts/"+accountID+"/virtual-accounts/"+virtualAccountID+"/earn-requests/in", bytes.NewBuffer(body))
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(EarningAccountMovement)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//WithdrawFundsFromEarningAccount This route will automatically move the requested funds from the "earning" sub account to the "outgoingEarn" sub account.
// Our backoffice will then process your request and your funds will be transfered back to the "funding" sub account.
//see: https://docs.trade.sheeldmatch.com/#withdraw-funds-from-earning-account
func (s *SheeldMarket) WithdrawFundsFromEarningAccount(accountID, virtualAccountID string, val EarningAccountMovementRequest) (*EarningAccountMovement, error) {
	body, err := json.Marshal(val)
	if err != nil {
		return nil, fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"accounts/"+accountID+"/virtual-accounts/"+virtualAccountID+"/earn-requests/out", bytes.NewBuffer(body))
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(EarningAccountMovement)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAllVirtualAccountsEarnRequests This route allow the user to get the earn requests for all virtual accounts owned by the specified account.
//see: https://docs.trade.sheeldmatch.com/#get-all-virtual-accounts-earn-requests
func (s *SheeldMarket) GetAllVirtualAccountsEarnRequests(accountId string, val GetVirtualAccountsEarnRequests) (*GetVirtualAccountsEarnResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/earn-requests"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Status != nil {
		q.Set("status", val.Status.String())
	}
	if val.Type != nil {
		q.Set("type", val.Type.String())
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetVirtualAccountsEarnResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountEarnRequests This route allow the user to get a specific virtual account's earn requests.
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-earn-requests
func (s *SheeldMarket) GetVirtualAccountEarnRequests(accountId, virtualAccountId string, val GetVirtualAccountsEarnRequests) (*GetVirtualAccountsEarnResponse, error) {
	path := s.baseUrl + "/accounts/" + accountId + "/virtual-accounts/" + virtualAccountId + "/earn-requests"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Status != nil {
		q.Set("status", val.Status.String())
	}
	if val.Type != nil {
		q.Set("type", val.Type.String())
	}
	u.RawQuery = q.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	// TODO - pagination
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetVirtualAccountsEarnResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetEarnRequest This route allow the user to get a specific earn request data.
//see: https://docs.trade.sheeldmatch.com/#get-specific-earn-request
func (s *SheeldMarket) GetEarnRequest(accountId, virtualAccountId, earnRequestID string) (*EarningAccountMovement, error) {
	// TODO - How to speficy which earn request? Question asked to SheeldMarket
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"/accounts/"+accountId+"/virtual-accounts/"+virtualAccountId+"/earn-requests", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(EarningAccountMovement)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAllVirtualAccountsEarningInterests This route allow the user to get the accumulated earning interests for all virtual accounts owned by the specified account.
// The values returned represents the total lifetime interests earned via the Earn product (and not the total funds present on the "earning" sub account).
//see: https://docs.trade.sheeldmatch.com/#get-all-virtual-accounts-earning-interests
func (s *SheeldMarket) GetAllVirtualAccountsEarningInterests(accountId string) (*GetAllVirtualAccountsEarningInterestsResponse, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"/accounts/"+accountId+"/virtual-accounts/earning-interests", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetAllVirtualAccountsEarningInterestsResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetVirtualAccountEarningInterests This route allow the user to get the accumulated earning interests for all virtual accounts owned by the specified account.
// The values returned represents the total lifetime interests earned via the Earn product (and not the total funds present on the "earning" sub account).
//see: https://docs.trade.sheeldmatch.com/#get-specific-virtual-account-39-s-earning-interests
func (s *SheeldMarket) GetVirtualAccountEarningInterests(accountId, virtualAccountId string) (*VirtualAccountsEarningInterests, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"/accounts/"+accountId+"/virtual-accounts/"+virtualAccountId+"/earning-interests", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(VirtualAccountsEarningInterests)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}
