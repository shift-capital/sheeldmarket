package sheeldmarket

import (
	"errors"
)

func testGetAvailableInstruments(s *SheeldMarket) error {
	res, err := s.GetAvailableInstruments()
	if err != nil {
		return errors.New("GetAvailableInstruments: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAvailableInstruments() res == nil")
	}
	return nil
}

func testGetAvailableVenues(s *SheeldMarket) error {
	res, err := s.GetAvailableVenues()
	if err != nil {
		return errors.New("GetAvailableVenues: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAvailableVenues() res == nil")
	}
	return nil
}

func testGetAvailableAssets(s *SheeldMarket) error {
	res, err := s.GetAvailableAssets()
	if err != nil {
		return errors.New("GetAvailableAssets: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAvailableAssets() res == nil")
	}
	return nil
}

func testGetAvailableNetworks(s *SheeldMarket) error {
	res, err := s.GetAvailableNetworks()
	if err != nil {
		return errors.New("GetAvailableNetworks: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAvailableNetworks() res == nil")
	}
	return nil
}
