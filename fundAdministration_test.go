package sheeldmarket

import "errors"

func testGetActivity(s *SheeldMarket, val GetActivityRequest) error {
	res, err := s.GetActivity(val)
	if err != nil {
		return errors.New("GetActivity: " + err.Error())
	}
	if res == nil {
		return errors.New("GetActivity() res == nil")
	}
	return nil
}

func testGetActivityReport(s *SheeldMarket, val GetActivityRequest) error {
	res, err := s.GetActivityReport(val)
	if err != nil {
		return errors.New("GetActivityReport: " + err.Error())
	}
	if res == nil {
		return errors.New("GetActivityReport() res == nil")
	}
	return nil
}

func testGetBalances(s *SheeldMarket, val GetBalancesRequest) error {
	res, err := s.GetBalances(val)
	if err != nil {
		return errors.New("GetBalances: " + err.Error())
	}
	if res == nil {
		return errors.New("GetBalances() res == nil")
	}
	return nil
}

func testGetOrdersHistory(s *SheeldMarket, val GetOrdersHistoryRequest) error {
	res, err := s.GetOrdersHistory(val)
	if err != nil {
		return errors.New("GetOrdersHistory: " + err.Error())
	}
	if res == nil {
		return errors.New("GetOrdersHistory() res == nil")
	}
	return nil
}

func testGetOrdersHistoryReport(s *SheeldMarket, val GetOrdersHistoryRequest) error {
	res, err := s.GetOrdersHistoryReport(val)
	if err != nil {
		return errors.New("GetOrdersHistoryReport: " + err.Error())
	}
	if res == nil {
		return errors.New("GetOrdersHistoryReport() res == nil")
	}
	return nil
}

func testGetTradesHistory(s *SheeldMarket, val GetTradesHistoryRequest) error {
	res, err := s.GetTradesHistory(val)
	if err != nil {
		return errors.New("GetTradesHistory: " + err.Error())
	}
	if res == nil {
		return errors.New("GetTradesHistory() res == nil")
	}
	return nil
}

func testGetTradesHistoryReport(s *SheeldMarket, val GetTradesHistoryRequest) error {
	res, err := s.GetTradesHistoryReport(val)
	if err != nil {
		return errors.New("GetTradesHistoryReport: " + err.Error())
	}
	if res == nil {
		return errors.New("GetTradesHistoryReport() res == nil")
	}
	return nil
}
