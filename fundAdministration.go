package sheeldmarket

import (
	"fmt"
	"net/http"
	"net/url"
)

//GetActivity This route allows users to retrieve their account and subAccount activity.
// The returned data is not ordered in any way for performance reasons.
//see: https://docs.trade.sheeldmatch.com/#get-activity
func (s *SheeldMarket) GetActivity(val GetActivityRequest) (*GetActivityResponse, error) {
	path := s.baseUrl + "user/activity"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Account != nil {
		q.Set("account", *val.Account)
	}
	if val.SubAccount != nil {
		q.Set("subAccount", *val.SubAccount)
	}
	if val.Asset != nil {
		q.Set("asset", *val.Asset)
	}
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetActivityResponse)
	err = s.do(req, &resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetActivityReport This route allows users to retrieve their account and subAccount activity as a CSV file.
// The returned data is not ordered in any way for performance reasons.
//see: https://docs.trade.sheeldmatch.com/#get-activity-report
func (s *SheeldMarket) GetActivityReport(val GetActivityRequest) ([]byte, error) {
	path := s.baseUrl + "user/activity/report"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Account != nil {
		q.Set("account", *val.Account)
	}
	if val.SubAccount != nil {
		q.Set("subAccount", *val.SubAccount)
	}
	if val.Asset != nil {
		q.Set("asset", *val.Asset)
	}
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp, err := s.doRaw(req)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return *resp, nil
}

//GetBalances This route allows users to retrieve their account and subAccount balances.
//see: https://docs.trade.sheeldmatch.com/#get-mpid-balances
func (s *SheeldMarket) GetBalances(val GetBalancesRequest) (*GetBalancesResponse, error) {
	path := s.baseUrl + "user/balances"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Account != nil {
		q.Set("account", *val.Account)
	}
	if val.Asset != nil {
		q.Set("asset", *val.Asset)
	}
	if val.Date != nil {
		q.Set("date", fmt.Sprintf("%d", *val.Date))
	}
	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetBalancesResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetOrdersHistory This route allows users to retrieve their order activity
//see: https://docs.trade.sheeldmatch.com/#orders-history
func (s *SheeldMarket) GetOrdersHistory(val GetOrdersHistoryRequest) (*GetOrdersHistoryResponse, error) {
	path := s.baseUrl + "user/orders"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Account != nil {
		q.Set("account", *val.Account)
	}
	if val.MPID != nil {
		q.Set("mpid", *val.MPID)
	}
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.HideClosed != nil {
		q.Set("hideClosed", fmt.Sprintf("%t", *val.HideClosed))
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetOrdersHistoryResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetOrdersHistoryReport This route allows users to retrieve a CSV report of their order activity The returned data is not ordered in any way for performance reasons.
//see: https://docs.trade.sheeldmatch.com/#orders-history-report
func (s *SheeldMarket) GetOrdersHistoryReport(val GetOrdersHistoryRequest) ([]byte, error) {
	path := s.baseUrl + "user/orders/report"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Account != nil {
		q.Set("account", *val.Account)
	}
	if val.MPID != nil {
		q.Set("mpid", *val.MPID)
	}
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}
	if val.HideClosed != nil {
		q.Set("hideClosed", fmt.Sprintf("%t", *val.HideClosed))
	}
	if val.Order != nil {
		q.Set("order", val.Order.String())
	}
	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp, err := s.doRaw(req)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return *resp, nil
}

//GetTradesHistory This route allows users to retrieve their trade activity
//see: https://docs.trade.sheeldmatch.com/#trades-history
func (s *SheeldMarket) GetTradesHistory(val GetTradesHistoryRequest) (*GetTradesHistoryResponse, error) {
	path := s.baseUrl + "user/trades"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Account != nil {
		q.Set("account", *val.Account)
	}
	if val.MPID != nil {
		q.Set("mpid", *val.MPID)
	}
	if val.Asset != nil {
		q.Set("asset", *val.Asset)
	}
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetTradesHistoryResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetTradesHistoryReport This route allows users to retrieve a CSV report of their trade activity The returned data is not ordered in any way for performance reasons.
//see: https://docs.trade.sheeldmatch.com/#trades-history-report
func (s *SheeldMarket) GetTradesHistoryReport(val GetTradesHistoryRequest) ([]byte, error) {
	path := s.baseUrl + "user/trades/report"
	u, _ := url.Parse(path)
	q := u.Query()
	if val.Account != nil {
		q.Set("account", *val.Account)
	}
	if val.MPID != nil {
		q.Set("mpid", *val.MPID)
	}
	if val.Asset != nil {
		q.Set("asset", *val.Asset)
	}
	if val.Start != nil {
		q.Set("start", fmt.Sprintf("%d", *val.Start))
	}
	if val.End != nil {
		q.Set("end", fmt.Sprintf("%d", *val.End))
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp, err := s.doRaw(req)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return *resp, nil
}
