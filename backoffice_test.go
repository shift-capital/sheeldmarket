package sheeldmarket

import "errors"

func testGetMPIDBalances(s *SheeldMarket) error {
	res, err := s.GetMPIDBalances()
	if err != nil {
		return errors.New("GetMPIDBalances: " + err.Error())
	}
	if res == nil {
		return errors.New("GetMPIDBalances() res == nil")
	}
	return nil
}

func testGetUserInformation(s *SheeldMarket) error {
	res, err := s.GetUserInformation()
	if err != nil {
		return errors.New("GetUserInformation: " + err.Error())
	}
	if res == nil {
		return errors.New("GetUserInformation() res == nil")
	}
	return nil
}

func testGetDepositAddresses(s *SheeldMarket, accountID string) error {
	res, err := s.GetDepositAddresses(accountID)
	if err != nil {
		return errors.New("GetDepositAddresses: " + err.Error())
	}
	if res == nil {
		return errors.New("GetDepositAddresses() res == nil")
	}
	return nil
}

func testGetWithdrawalAddresses(s *SheeldMarket, accountID string) error {
	res, err := s.GetWithdrawalAddresses(accountID)
	if err != nil {
		return errors.New("GetWithdrawalAddresses: " + err.Error())
	}
	if res == nil {
		return errors.New("GetWithdrawalAddresses() res == nil")
	}
	return nil
}

func testRequestWithdrawal(s *SheeldMarket, accountID string, val RequestWithdrawalRequest) error {
	res, err := s.RequestWithdrawal(accountID, val)
	if err != nil {
		return errors.New("RequestWithdrawal: " + err.Error())
	}
	if res == nil {
		return errors.New("RequestWithdrawal() res == nil")
	}
	return nil
}

func testGetWithdrawalRequests(s *SheeldMarket, accountID string) error {
	res, err := s.GetWithdrawalRequests(accountID)
	if err != nil {
		return errors.New("GetWithdrawalRequests: " + err.Error())
	}
	if res == nil {
		return errors.New("GetWithdrawalRequests() res == nil")
	}
	return nil
}

func testGetDepositsHistory(s *SheeldMarket, accountID string) error {
	res, err := s.GetDepositsHistory(accountID)
	if err != nil {
		return errors.New("GetDepositsHistory: " + err.Error())
	}
	if res == nil {
		return errors.New("GetDepositsHistory() res == nil")
	}
	return nil
}

func testGetWithdrawalsHistory(s *SheeldMarket, accountID string) error {
	res, err := s.GetWithdrawalsHistory(accountID)
	if err != nil {
		return errors.New("GetWithdrawalsHistory: " + err.Error())
	}
	if res == nil {
		return errors.New("GetWithdrawalsHistory() res == nil")
	}
	return nil
}

func testRequestTransfer(s *SheeldMarket, accountID string, val RequestTransferRequest) error {
	res, err := s.RequestTransfer(accountID, val)
	if err != nil {
		return errors.New("RequestTransfer: " + err.Error())
	}
	if res == nil {
		return errors.New("RequestTransfer() res == nil")
	}
	return nil
}

func testGetTransfersHistory(s *SheeldMarket, accountID string) error {
	res, err := s.GetTransfersHistory(accountID)
	if err != nil {
		return errors.New("GetTransfersHistory: " + err.Error())
	}
	if res == nil {
		return errors.New("GetTransfersHistory() res == nil")
	}
	return nil
}
