module gitlab.com/shift-capital/sheeldmarket

go 1.18

require (
	github.com/gorilla/websocket v1.5.0
	github.com/joho/godotenv v1.4.0
	moul.io/http2curl v1.0.0
)

require github.com/smartystreets/goconvey v1.7.2 // indirect
