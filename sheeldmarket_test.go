package sheeldmarket

import (
	"fmt"
	"os"
	"reflect"
	"testing"
	"time"

	"github.com/joho/godotenv"
)

func TestNewSheeldMarket(t *testing.T) {
	type args struct {
		cnf *SheeldMarketConfig
	}
	tests := []struct {
		name    string
		args    args
		want    reflect.Type
		wantErr bool
	}{
		{
			name: "Test SheeldMarket",
			args: args{cnf: &SheeldMarketConfig{
				Test:     true,
				Username: "username",
				Password: "password",
				MPID:     "mpid",
			}},
			wantErr: false,
			want:    reflect.TypeOf(&SheeldMarket{}),
		},
		// {
		// 	name: "SheeldMarket",
		// 	args: args{cnf: &SheeldMarketConfig{
		// 		Test:     false,
		// 		Username: "username",
		// 		Password: "password",
		// 		MPID:     "mpid",
		// 	}},
		// 	wantErr: false,
		// 	want:    reflect.TypeOf(&SheeldMarket{}),
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewSheeldMarket(tt.args.cnf)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewSheeldMarket() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if reflect.TypeOf(got) != tt.want {
				t.Errorf("NewSheeldMarket() got = %T, want %s", got, tt.want)
			}
		})
	}
}

func TestWorkflowSheeldMarket(t *testing.T) {
	err := godotenv.Load()
	if err != nil {
		t.Error(err)
	}
	username := os.Getenv("TEST_SHEELDMARKET_USERNAME")
	if username == "" {
		t.Error("env TEST_SHEELDMARKET_USERNAME required")
		return
	}
	password := os.Getenv("TEST_SHEELDMARKET_PASSWORD")
	if password == "" {
		t.Error("env TEST_SHEELDMARKET_PASSWORD required")
		return
	}
	mpid := os.Getenv("TEST_SHEELDMARKET_MPID")
	if mpid == "" {
		t.Error("env TEST_SHEELDMARKET_MPID required")
		return
	}
	s, err := NewSheeldMarket(&SheeldMarketConfig{
		Test:     true,
		Username: username,
		Password: password,
		MPID:     mpid,
	})
	if err != nil {
		t.Errorf("NewSheeldMarket: %v", err)
		return
	}

	//******************************************************************
	//	Universe
	// ******************************************************************
	// testUniverse(t, s)

	//******************************************************************
	//	Back Office
	//******************************************************************
	// testBackoffice(t, s)

	//******************************************************************
	// Fund Administration
	//******************************************************************
	// testFundAdministration(t, s)

	//******************************************************************
	// Trading
	//******************************************************************
	testTrading(t, s)

	//******************************************************************
	// Market Analytics
	// ******************************************************************
	// testMarketAnalytics(t, s)

	//******************************************************************
	// Virtual Accounts
	// ******************************************************************
	// NOT TESTED - We do not have access to this section of the API
	// testVirtualAccounts(t, s)

}

func testUniverse(t *testing.T, s *SheeldMarket) {

	//******************************************************************
	// Instruments
	//******************************************************************
	err := testGetAvailableInstruments(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Venues
	//******************************************************************
	err = testGetAvailableVenues(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Assets
	//******************************************************************
	err = testGetAvailableAssets(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Networks
	//******************************************************************
	err = testGetAvailableNetworks(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)
}

func testBackoffice(t *testing.T, s *SheeldMarket) {

	//******************************************************************
	// MPID Balances
	//******************************************************************
	err := testGetMPIDBalances(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// User Information
	//******************************************************************
	err = testGetUserInformation(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	accountID := "1f8a7f49-8af6-499b-9a79-d504b585bf15"

	//******************************************************************
	// Withdrawals
	//******************************************************************
	err = testGetWithdrawalAddresses(s, accountID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)
	/*
		withdrawalRequest := RequestWithdrawalRequest{
			AssetID:     "USDT",
			Address:     "xxxxx",
			Quantity:    "1",
			Description: "test withdrawal",
			IncludeFees: false,
		}
		err = testRequestWithdrawal(s, accountID, withdrawalRequest)
		if err != nil {
			t.Error(err)
			return
		}
		// Sleep for auth rate
		// time.Sleep(3 * time.Second)
	*/
	err = testGetWithdrawalRequests(s, accountID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetWithdrawalsHistory(s, accountID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Deposits
	//******************************************************************

	err = testGetDepositAddresses(s, accountID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetDepositsHistory(s, accountID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Transfers
	//******************************************************************
	description := "test transfer"
	transferRequest := RequestTransferRequest{
		AssetID:       "USDT",
		SourceID:      "SFTA",
		DestinationID: "funding",
		Quantity:      100,
		Description:   &description,
	}
	err = testRequestTransfer(s, accountID, transferRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetTransfersHistory(s, accountID)
	if err != nil {
		t.Error(err)
		return
	}
}

func testFundAdministration(t *testing.T, s *SheeldMarket) {

	//******************************************************************
	// Activity
	//******************************************************************
	account := "1f8a7f49-8af6-499b-9a79-d504b585bf15"
	subAccount := "SFTA"
	asset := "USDT"
	start := int64(1257894000)
	end := time.Now().Unix()
	getActivityRequest := GetActivityRequest{
		Account:    &account,
		SubAccount: &subAccount,
		Asset:      &asset,
		Start:      &start,
		End:        &end,
	}
	err := testGetActivity(s, getActivityRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetActivityReport(s, getActivityRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Balances
	//******************************************************************

	date := time.Now().Unix()
	getBalancesRequest := GetBalancesRequest{
		Account: &account,
		Asset:   &asset,
		Date:    &date,
	}
	err = testGetBalances(s, getBalancesRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Orders
	//******************************************************************

	mpid := s.cnf.MPID
	hideClosed := false
	order := SortOrderAsc
	getOrdersHistoryRequest := GetOrdersHistoryRequest{
		Account:    &account,
		MPID:       &mpid,
		Start:      &start,
		End:        &end,
		HideClosed: &hideClosed,
		Order:      &order,
	}
	err = testGetOrdersHistory(s, getOrdersHistoryRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetOrdersHistoryReport(s, getOrdersHistoryRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Trades
	//******************************************************************

	getTradesHistoryRequest := GetTradesHistoryRequest{
		Account: &account,
		MPID:    &mpid,
		// Asset:   &asset,
		Start: &start,
		End:   &end,
	}
	err = testGetTradesHistory(s, getTradesHistoryRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetTradesHistoryReport(s, getTradesHistoryRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)
}

func testTrading(t *testing.T, s *SheeldMarket) {

	//******************************************************************
	// DMA Orders
	//******************************************************************

	// IoC
	token := fmt.Sprintf("%d", time.Now().Unix())
	addDMAOrderRequest := AddDMAOrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideBuy,
		Volume:     0.1,
		Price:      47002,
		Venue:      "FTX",
	}
	err := testAddDMAOrder(s, DMAOrderTypeIoC, addDMAOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// GTC
	token = fmt.Sprintf("%d", time.Now().Unix())
	addDMAOrderRequest = AddDMAOrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideBuy,
		Volume:     0.1,
		Price:      47002,
		Venue:      "FTX",
	}
	err = testAddDMAOrder(s, DMAOrderTypeGTC, addDMAOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// DayOrder
	token = fmt.Sprintf("%d", time.Now().Unix())
	saveToken := token
	addDMAOrderRequest = AddDMAOrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideSell,
		Volume:     0.1,
		Price:      47002,
		Venue:      "FTX",
	}
	err = testAddDMAOrder(s, DMAOrderTypeDayOrder, addDMAOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// POD
	token = fmt.Sprintf("%d", time.Now().Unix())
	saveToken2 := token
	addDMAOrderRequest = AddDMAOrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideSell,
		Volume:     0.1,
		Price:      47002,
		Venue:      "FTX",
	}
	err = testAddDMAOrder(s, DMAOrderTypePOD, addDMAOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// SOR Orders
	//******************************************************************

	// ISO
	token = fmt.Sprintf("%d", time.Now().Unix())
	priority := SorOrderPriorityBoth
	addSOROrderRequest := AddSOROrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideBuy,
		Volume:     0.1,
		Price:      47002,
		Priority:   &priority,
	}
	err = testAddSOROrder(s, SOROrderTypeISO, addSOROrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// IDO
	token = fmt.Sprintf("%d", time.Now().Unix())
	priority = SorOrderPriorityBoth
	addSOROrderRequest = AddSOROrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideBuy,
		Volume:     0.1,
		Price:      47002,
		Priority:   &priority,
	}
	err = testAddSOROrder(s, SOROrderTypeIDO, addSOROrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// IGO
	token = fmt.Sprintf("%d", time.Now().Unix())
	priority = SorOrderPriorityBoth
	addSOROrderRequest = AddSOROrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideSell,
		Volume:     0.1,
		Price:      47002,
		Priority:   &priority,
	}
	err = testAddSOROrder(s, SOROrderTypeIGO, addSOROrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// IPO
	token = fmt.Sprintf("%d", time.Now().Unix())
	priority = SorOrderPriorityBoth
	addSOROrderRequest = AddSOROrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideSell,
		Volume:     0.1,
		Price:      47002,
		Priority:   &priority,
	}
	err = testAddSOROrder(s, SOROrderTypeIPO, addSOROrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// DSA Orders
	//******************************************************************
	// VIO
	token = fmt.Sprintf("%d", time.Now().Unix())
	participation := int64(1)
	addDSAOrderRequest := AddDSAOrderRequest{
		Token:         token,
		Instrument:    "BTC-USDT.SPOT",
		Side:          OrderSideBuy,
		Volume:        1,
		Price:         47002,
		Participation: &participation,
	}

	err = testAddDSAOrder(s, DSAOrderTypeVIO, addDSAOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// TWAP

	token = fmt.Sprintf("%d", time.Now().Unix())
	lifespan := int64(75)
	addDSAOrderRequest = AddDSAOrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideBuy,
		Volume:     0.1,
		Price:      47002,
		Lifespan:   &lifespan,
	}

	err = testAddDSAOrder(s, DSAOrderTypeTWAP, addDSAOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// VWAP

	token = fmt.Sprintf("%d", time.Now().Unix())
	addDSAOrderRequest = AddDSAOrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideSell,
		Volume:     0.1,
		Price:      47002,
		Lifespan:   &lifespan,
	}
	err = testAddDSAOrder(s, DSAOrderTypeVWAP, addDSAOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// TMO Orders
	//******************************************************************

	// PTO
	token = fmt.Sprintf("%d", time.Now().Unix())
	addTMOOrderRequest := AddTMOOrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideBuy,
		Volume:     0.1,
		Price:      47002,
	}
	err = testAddTMOOrder(s, TMOOrderTypePTO, addTMOOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// VTO
	token = fmt.Sprintf("%d", time.Now().Unix())
	stop := float64(47010)
	addTMOOrderRequest = AddTMOOrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideSell,
		Volume:     0.1,
		Price:      47002,
		Stop:       &stop,
	}

	err = testAddTMOOrder(s, TMOOrderTypeVTO, addTMOOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// SIO
	token = fmt.Sprintf("%d", time.Now().Unix())
	anchor := TMOAnchorBid
	addTMOOrderRequest = AddTMOOrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideSell,
		Volume:     0.1,
		Price:      47002,
		Stop:       &stop,
		Achor:      &anchor,
	}

	err = testAddTMOOrder(s, TMOOrderTypeSIO, addTMOOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	// TPO
	token = fmt.Sprintf("%d", time.Now().Unix())
	addTMOOrderRequest = AddTMOOrderRequest{
		Token:      token,
		Instrument: "BTC-USDT.SPOT",
		Side:       OrderSideBuy,
		Volume:     0.1,
		Price:      47002,
		Stop:       &stop,
		Achor:      &anchor,
	}
	err = testAddTMOOrder(s, TMOOrderTypeTPO, addTMOOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Order Operations
	//******************************************************************

	modifyOrderRequest := ModifyOrderRequest{
		Instrument: "BTC-USDT.SPOT",
		Volume:     0.1,
		Price:      47003,
	}
	err = testModifyOrder(s, saveToken, modifyOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	reduceOrderRequest := ReduceOrderRequest{
		Instrument: "BTC-USDT.SPOT",
		Volume:     0.099,
	}
	err = testReduceOrder(s, saveToken2, reduceOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetOrderStatus(s, saveToken)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetOpenOrders(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	instrument := "BTC-USDT.SPOT"
	start := int64(1257894000)
	end := time.Now().Unix()
	hideClosed := false
	getOrdersRequest := GetOrdersRequest{
		Instrument: &instrument,
		Start:      &start,
		End:        &end,
		HideClosed: &hideClosed,
	}
	err = testGetOrders(s, getOrdersRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testCancelOrder(s, saveToken2)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetExecutions(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testMassCancelOrders(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testSuspendTrading(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testResumeTrading(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)
}

func testMarketAnalytics(t *testing.T, s *SheeldMarket) {

	//******************************************************************
	// Analytic Universe
	//******************************************************************

	err := testGetAnalyticUniverse(s)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Trades Average Size
	//******************************************************************

	venue := "Aggregated"
	lookback := "1d"
	geography := "World"
	dayOfWeek := "AllWeek"
	analyticParameters := AnalyticParameters{
		Instrument: "BTC-USDT.SPOT",
		Venue:      &venue,
		Lookback:   &lookback,
		Geography:  &geography,
		DayOfWeek:  &dayOfWeek,
	}
	err = testGetTradesAverageSize(s, analyticParameters)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Daily Average Volume
	//******************************************************************

	err = testGetDailyAverageVolume(s, analyticParameters)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Money Flow
	//******************************************************************

	err = testGetMoneyFlow(s, analyticParameters)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Annualized Volatility
	//******************************************************************

	err = testGetAnualizedVolatility(s, analyticParameters)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	time.Sleep(3 * time.Second)

	//******************************************************************
	// Estimated Seasonality
	//******************************************************************

	lookahead := "1d"

	analyticParameters = AnalyticParameters{
		Instrument: "BTC-USDT.SPOT",
		Venue:      &venue,
		Lookahead:  &lookahead,
	}
	err = testGetEstimatedSeasonality(s, analyticParameters)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	time.Sleep(3 * time.Second)

	//******************************************************************
	// Market Impact
	//******************************************************************

	impact := 1.0
	volume := 100000.0

	// volume + impact
	getMarketImpactRequest := GetMarketImpactRequest{
		Instrument: "BTC-USDT.SPOT",
		Impact:     &impact,
		Volume:     &volume,
	}
	err = testGetMarketImpact(s, getMarketImpactRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	time.Sleep(3 * time.Second)

	// lookahead + volume
	getMarketImpactRequest = GetMarketImpactRequest{
		Instrument: "BTC-USDT.SPOT",
		Lookahead:  &lookahead,
		Volume:     &volume,
	}
	err = testGetMarketImpact(s, getMarketImpactRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	time.Sleep(3 * time.Second)

	// lookahead + impact
	getMarketImpactRequest = GetMarketImpactRequest{
		Instrument: "BTC-USDT.SPOT",
		Lookahead:  &lookahead,
		Impact:     &impact,
	}
	err = testGetMarketImpact(s, getMarketImpactRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	time.Sleep(3 * time.Second)

}

func testVirtualAccounts(t *testing.T, s *SheeldMarket) {

	//******************************************************************
	// Virtual Accounts Information
	//******************************************************************

	accountID := "1f8a7f49-8af6-499b-9a79-d504b585bf15"
	virtualAccountID := "1f8a7f49-8af6-499b-9a79-d504b585bf15"
	email := "lucas@shiftcap.co"

	err := testGetAllVirtualAccounts(s, accountID, &email)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetVirtualAccountInformation(s, accountID, virtualAccountID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Withdrawals
	//******************************************************************
	memo := "test memo"
	addWithdrawalAddressRequest := AddWithdrawalAddressRequest{
		Value:       "0x00test",
		Description: "test withdrawal address",
		NetworkID:   "1",
		Memo:        &memo,
		Asset:       []string{"USDT"},
	}
	err = testAddWithdrawalAddress(s, accountID, virtualAccountID, addWithdrawalAddressRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	assetID := "USDT"
	err = testGetAllWithdrawalAddresses(s, accountID, &assetID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetSpecificVirtualAccountWithdrawalAddresses(s, accountID, virtualAccountID, &assetID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	description := "test withdrawal"
	requestWithdrawalFromVirtualAccountRequest := RequestWithdrawalFromVirtualAccountRequest{
		AssetID:     assetID,
		AddressID:   "8e839782-e616-419f-a10a-aed43cf9e29a",
		Quantity:    1,
		Description: &description,
	}
	err = testRequestWithdrawalFromVirtualAccount(s, accountID, virtualAccountID, requestWithdrawalFromVirtualAccountRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	status := VirtualAccountWithdrawalStatusApproved
	err = testGetAllWithdrawalRequests(s, accountID, &status, &assetID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetVirtualAccountWithdrawalRequests(s, accountID, virtualAccountID, &status, &assetID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	withdrawalRequestID := "???"
	err = testGetWithdrawalRequest(s, accountID, virtualAccountID, withdrawalRequestID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	start := int64(1257894000)
	end := time.Now().Unix()
	order := SortOrderAsc
	getAllWithdrawalsRequest := GetAllWithdrawalsRequest{
		Start:   &start,
		End:     &end,
		AssetID: &assetID,
		Order:   &order,
	}
	err = testGetAllWithdrawals(s, accountID, getAllWithdrawalsRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetVirtualAccountWithdrawals(s, accountID, virtualAccountID, getAllWithdrawalsRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Deposits
	//******************************************************************

	err = testGetAllDepositAddresses(s, accountID, &assetID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetSpecificVirtualAccountDepositAddresses(s, accountID, virtualAccountID, &assetID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	getAllDepositsRequest := GetAllDepositsRequest{
		Start:   &start,
		End:     &end,
		AssetID: &assetID,
		Order:   &order,
	}
	err = testGetAllDeposits(s, accountID, getAllDepositsRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetVirtualAccountDeposits(s, accountID, virtualAccountID, getAllDepositsRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Transfers
	//******************************************************************

	description = "test transfer"
	requestTransferFromVirtualAccountRequest := RequestTransferFromVirtualAccountRequest{
		AssetID:     assetID,
		Source:      "funding",
		Destination: "SFTA",
		Quantity:    1,
		Description: &description,
	}
	err = testRequestTransferFromVirtualAccount(s, accountID, virtualAccountID, requestTransferFromVirtualAccountRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetAllTransferRequests(s, accountID, &assetID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetVirtualAccountTransferRequests(s, accountID, virtualAccountID, &assetID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	transferRequestID := "???"
	err = testGetTransferRequest(s, accountID, virtualAccountID, transferRequestID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Orders
	//******************************************************************

	instrument := "BTC-USDT.SPOT"
	hideClosed := false
	side := GeneralOrderSideBuy
	getOrdersRequest := GetOrdersRequest{
		Start:      &start,
		End:        &end,
		Instrument: &instrument,
		Side:       &side,
		HideClosed: &hideClosed,
		Order:      &order,
	}
	err = testGetAllOrders(s, accountID, getOrdersRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetVirtualAccountOrders(s, accountID, virtualAccountID, getOrdersRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	getVirtualAccountOrderRequest := GetVirtualAccountOrderRequest{
		VirtualAccountID: virtualAccountID,
		MPID:             "SFTA",
		Token:            "TODO",
		Day:              "2022-05-29",
	}
	err = testGetVirtualAccountOrder(s, accountID, getVirtualAccountOrderRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Balances
	//******************************************************************

	subAccount := "funding"
	date := time.Now().Unix()
	getAllVirtualAccountsBalancesRequest := GetAllVirtualAccountsBalancesRequest{
		SubAccount: &subAccount,
		Date:       &date,
	}
	err = testGetAllVirtualAccountsBalances(s, accountID, getAllVirtualAccountsBalancesRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	getVirtualAccountBalancesRequest := GetVirtualAccountBalancesRequest{
		Date: &date,
	}
	err = testGetVirtualAccountBalances(s, accountID, virtualAccountID, getVirtualAccountBalancesRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	activityType := VirtualAccountActivityTypeDeposit
	getVirtualAccountActivityRequest := GetVirtualAccountActivityRequest{
		Start:      &start,
		End:        &end,
		SubAccount: &subAccount,
		Type:       &activityType,
		AssetID:    &assetID,
		Order:      &order,
	}
	err = testGetAllVirtualAccountsActivity(s, accountID, getVirtualAccountActivityRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetVirtualAccountActivity(s, accountID, virtualAccountID, getVirtualAccountActivityRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	mpid := "SFTA"
	getVirtualAccountsTradesRequest := GetVirtualAccountsTradesRequest{
		Start: &start,
		End:   &end,
		MPID:  &mpid,
		Order: &order,
	}
	err = testGetAllVirtualAccountsTrades(s, accountID, getVirtualAccountsTradesRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetVirtualAccountTrades(s, accountID, virtualAccountID, getVirtualAccountsTradesRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	//******************************************************************
	// Earning
	//******************************************************************

	earningAccountMovementRequest := EarningAccountMovementRequest{
		AssetID:  "USDT",
		Quantity: 1,
	}
	err = testWithdrawFundsFromEarningAccount(s, accountID, virtualAccountID, earningAccountMovementRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testDepositFundsToEarningAccount(s, accountID, virtualAccountID, earningAccountMovementRequest)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	earnRequestStatus := EarningAccountMovementStatusAccepted
	earnRequestType := EarnRequestTypeIncoming
	getVirtualAccountsEarnRequests := GetVirtualAccountsEarnRequests{
		Status: &earnRequestStatus,
		Type:   &earnRequestType,
	}
	err = testGetAllVirtualAccountsEarnRequests(s, accountID, getVirtualAccountsEarnRequests)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetVirtualAccountEarnRequests(s, accountID, virtualAccountID, getVirtualAccountsEarnRequests)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetAllVirtualAccountsEarningInterests(s, accountID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)

	err = testGetVirtualAccountEarningInterests(s, accountID, virtualAccountID)
	if err != nil {
		t.Error(err)
		return
	}
	// Sleep for auth rate
	// time.Sleep(3 * time.Second)
}
