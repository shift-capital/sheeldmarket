package sheeldmarket

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"moul.io/http2curl"
)

func (s *SheeldMarket) do(req *http.Request, resp interface{}) error {

	req.Header.Set("Content-Type", "application/json")

	// FORMAT REQUEST AND PRINT
	command, _ := http2curl.GetCurlCommand(req)
	fmt.Println(command)

	res, err := s.client.Do(req)
	if err != nil {
		return fmt.Errorf("Do: %w", err)
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("ReadAll: %w", err)
	}
	fmt.Println(string(body))
	fmt.Println(res.StatusCode)
	err = s.error(body, res.StatusCode)
	if err != nil {
		return fmt.Errorf("error: %w", err)
	}
	if req.Method == http.MethodPost || req.Method == http.MethodPatch || req.Method == http.MethodDelete {
		err = s.errorPostPatchDelete(body, res.StatusCode)
		if err != nil {
			return fmt.Errorf("error: %w", err)
		}
		return nil
	}

	if resp == nil {
		return nil
	}
	err = json.Unmarshal(body, resp)
	if err != nil {
		return fmt.Errorf("Unmarshal: %w", err)
	}
	return nil
}

func (s *SheeldMarket) doRaw(req *http.Request) (*[]byte, error) {

	req.Header.Set("Content-Type", "application/json")

	// FORMAT REQUEST AND PRINT
	// command, _ := http2curl.GetCurlCommand(req)
	// fmt.Println(command)

	res, err := s.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("Do: %w", err)
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("ReadAll: %w", err)
	}

	return &body, nil
}

func (s *SheeldMarket) authenticate(req *http.Request) error {
	authToken, err := s.GenerateAuthToken()
	if err != nil {
		return fmt.Errorf("GenerateAuthToken: %w", err)
	}
	req.Header.Set("Authorization", "Bearer "+authToken.AuthToken)
	return nil
}

func (s *SheeldMarket) error(body []byte, status int) error {
	if status == http.StatusOK || status == http.StatusAccepted || status == http.StatusNoContent {
		return nil
	}
	err := new(APIError)
	e := json.Unmarshal(body, err)
	if e != nil {
		return fmt.Errorf("Unexpected response. Status: %d Body: %s", status, string(body))
	}
	return fmt.Errorf("Status: %d Error: %w", status, err)
}

func (s *SheeldMarket) errorPostPatchDelete(body []byte, status int) error {
	if status == http.StatusOK || status == http.StatusAccepted || status == http.StatusNoContent {
		return nil
	}
	e := string(body)
	if e == "" {
		return fmt.Errorf("Unexpected response. Status: %d Body empty", status)
	}
	return fmt.Errorf("Status: %d Error: %s", status, e)
}
