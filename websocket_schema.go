package sheeldmarket

import "strconv"

type MarketDataWSPathParams struct {
	Depth MarketDataDepth
	Pairs []PairAndVenue
}

type PairAndVenue struct {
	Pair  string
	Venue string
}

type MarketDataDepth int64

const (
	MarketDataDepth0  MarketDataDepth = 0
	MarketDataDepth1  MarketDataDepth = 1
	MarketDataDepth10 MarketDataDepth = 10
	MarketDataDepth25 MarketDataDepth = 25
)

// example wsurl="wss://sheeldmatch.com:6443/?depth=10&symbols=BTC-USDT.SPOT@Binance/ADA-BTC.PERP@OKEx"
func (m MarketDataWSPathParams) ToString() string {
	str := "?depth="
	str += strconv.FormatInt(int64(m.Depth), 10)
	if len(m.Pairs) > 0 {
		str += "&symbols="
	}
	for i, pair := range m.Pairs {
		if i > 0 {
			str += "/"
		}
		str += pair.Pair + "@" + pair.Venue
	}
	return str
}

type MarketDataWSMessage struct {
	Type       MarketDataWSMessageType `json:"type"`
	Venue      *string                 `json:"venue"`
	Instrument *string                 `json:"instrument"`
	Data       []interface{}           `json:"data"`
}

type MarketDataWSMessageType string

const (
	MarketDataWSMessageTypeH MarketDataWSMessageType = "H" //Trades Messages
	MarketDataWSMessageTypeL MarketDataWSMessageType = "L" //Trades Messages
	MarketDataWSMessageTypeB MarketDataWSMessageType = "B" //Order-Book Messages
	MarketDataWSMessageTypeS MarketDataWSMessageType = "S" //Order-Book Messages
	MarketDataWSMessageTypeF MarketDataWSMessageType = "F" //Funding Rate Messages
	MarketDataWSMessageTypeZ MarketDataWSMessageType = "Z" //Sequence-is-over Messages
	MarketDataWSMessageTypeU MarketDataWSMessageType = "U" //Micro-Structure Messages
)

const (
	UNDEFINED = iota
	ERROR
	TRADES
	ORDERBOOK
	FUNDINGRATE
)

type MarketDataStream struct {
	Type           int
	Trades         MarketDataWSTrades
	MicroStructure MarketDataWSMicroStructure
	OrderBooks     MarketDataWSOrderBooks
	FundingRate    MarketDataWSFundingRate
	Results        error
}

// Trades
type MarketDataWSTrades struct {
	Type       MarketDataWSMessageType `json:"type"`
	Venue      *string                 `json:"venue"`
	Instrument *string                 `json:"instrument"`
	Data       []MarketDataWSTradeData `json:"data"`
}

type MarketDataWSTradeData struct {
	Price  float64 `json:"price"`
	Volume float64 `json:"volume"`
	Time   string  `json:"time"`
}

// Micro Structure
type MarketDataWSMicroStructure struct {
	Type       MarketDataWSMessageType          `json:"type"`
	Venue      *string                          `json:"venue"`
	Instrument *string                          `json:"instrument"`
	Data       []MarketDataWSMicroStructureData `json:"data"`
}

type MarketDataWSMicroStructureData struct {
	Lot  int64  `json:"lot"`
	Tick int64  `json:"tick"`
	Time string `json:"time"`
}

// Order Books
type MarketDataWSOrderBooks struct {
	Type       MarketDataWSMessageType     `json:"type"`
	Venue      *string                     `json:"venue"`
	Instrument *string                     `json:"instrument"`
	Data       []MarketDataWSOrderBookData `json:"data"`
}

type MarketDataWSOrderBookData struct {
	Depth  int64   `json:"depth"`
	Price  float64 `json:"price"`
	Volume float64 `json:"volume"`
	Time   string  `json:"time"`
}

// Funding Rate
type MarketDataWSFundingRate struct {
	Type       MarketDataWSMessageType       `json:"type"`
	Venue      *string                       `json:"venue"`
	Instrument *string                       `json:"instrument"`
	Data       []MarketDataWSFundingRateData `json:"data"`
}

type MarketDataWSFundingRateData struct {
	FundingRate float64 `json:"funding_rate"`
	Settlement  string  `json:"settlement"`
	Time        string  `json:"time"`
}
