package sheeldmarket

import (
	"fmt"
	"net/http"
	"net/url"
)

//GetAnalyticUniverse The following analytics are consolidated data using our market data capture. The results may differ from other analytics tools using third party market data as SheeldMarket may filter some data deemed incorrect and/or suspicious.
//see: https://docs.trade.sheeldmatch.com/#market-analytics
func (s *SheeldMarket) GetAnalyticUniverse() (*AnalyticUniverse, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"analytics/universe", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(AnalyticUniverse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetTradesAverageSize This analytic gives the typical size of trades of a pair on a venue based on specific lookbacks (one day, one week, one month), for a specific geography (trading hours) and a specific day in week.
//see: https://docs.trade.sheeldmatch.com/#get-trades-average-size
func (s *SheeldMarket) GetTradesAverageSize(val AnalyticParameters) (*GetTradesAverageSizeResponse, error) {
	path := s.baseUrl + "analytics/trades"
	u, _ := url.Parse(path)
	q := u.Query()
	q.Set("instrument", val.Instrument)
	if val.Venue != nil {
		q.Set("venue", *val.Venue)
	}
	if val.Lookback != nil {
		q.Set("lookback", *val.Lookback)
	}
	if val.Geography != nil {
		q.Set("geography", *val.Geography)
	}
	if val.DayOfWeek != nil {
		q.Set("dayOfWeek", *val.DayOfWeek)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetTradesAverageSizeResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetDailyAverageVolume This analytic gives the average daily volume of a pair on a venue based on specific lookbacks (one day, one week, one month) for a specific geography (trading hours) and a specific day in week.
//see: https://docs.trade.sheeldmatch.com/#get-daily-average-volume
func (s *SheeldMarket) GetDailyAverageVolume(val AnalyticParameters) (*GetDailyAverageVolumeResponse, error) {
	path := s.baseUrl + "analytics/volume"
	u, _ := url.Parse(path)
	q := u.Query()
	q.Set("instrument", val.Instrument)
	if val.Venue != nil {
		q.Set("venue", *val.Venue)
	}
	if val.Lookback != nil {
		q.Set("lookback", *val.Lookback)
	}
	if val.Geography != nil {
		q.Set("geography", *val.Geography)
	}
	if val.DayOfWeek != nil {
		q.Set("dayOfWeek", *val.DayOfWeek)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetDailyAverageVolumeResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetMoneyFlow This analytic gives the ratio between buyers and sellers for a pair on a venue based on specific lookbacks (one day, one week, one month) for a specific geography (trading hours) and a specific day in week.
//see: https://docs.trade.sheeldmatch.com/#get-money-flow
func (s *SheeldMarket) GetMoneyFlow(val AnalyticParameters) (*GetMoneyFlowResponse, error) {
	path := s.baseUrl + "analytics/money-flow"
	u, _ := url.Parse(path)
	q := u.Query()
	q.Set("instrument", val.Instrument)
	if val.Venue != nil {
		q.Set("venue", *val.Venue)
	}
	if val.Lookback != nil {
		q.Set("lookback", *val.Lookback)
	}
	if val.Geography != nil {
		q.Set("geography", *val.Geography)
	}
	if val.DayOfWeek != nil {
		q.Set("dayOfWeek", *val.DayOfWeek)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetMoneyFlowResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetAnualizedVolatility This analytic estimates annualized volatility of a pair using SheeldMarket’s proprietary quantitative models.
//see: https://docs.trade.sheeldmatch.com/#get-annualized-volatility
func (s *SheeldMarket) GetAnualizedVolatility(val AnalyticParameters) (*GetAnualizedVolatilityResponse, error) {
	path := s.baseUrl + "analytics/volatility"
	u, _ := url.Parse(path)
	q := u.Query()
	q.Set("instrument", val.Instrument)
	if val.Venue != nil {
		q.Set("venue", *val.Venue)
	}
	if val.Lookback != nil {
		q.Set("lookback", *val.Lookback)
	}
	if val.Geography != nil {
		q.Set("geography", *val.Geography)
	}
	if val.DayOfWeek != nil {
		q.Set("dayOfWeek", *val.DayOfWeek)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetAnualizedVolatilityResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetEstimatedSeasonality This analytic estimates trade volume of a pair for specific lookahead (currently restricted to one day) using SheeldMarket’s proprietary quantitative models.
//see: https://docs.trade.sheeldmatch.com/#get-estimated-seasonality
func (s *SheeldMarket) GetEstimatedSeasonality(val AnalyticParameters) (*GetEstimatedSeasonalityResponse, error) {
	path := s.baseUrl + "analytics/seasonality"
	u, _ := url.Parse(path)
	q := u.Query()
	q.Set("instrument", val.Instrument)
	if val.Venue != nil {
		q.Set("venue", *val.Venue)
	}
	if val.Lookahead != nil {
		q.Set("lookahead", *val.Lookahead)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetEstimatedSeasonalityResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetMarketImpact This route can represent three different analytic depending on the params used.
// Examples of valid lookahead values: 2d, 3.5w, 1m;
// This route MUST take exactly 2 of the 3 optional parameters impact, volume and lookahead.
// The returned analytic depends on the 2 parameters supplied:
// volume + impact : estimates the time required to trade specific volumes ($100k, $500k, $1m, $5m, $10m) while staying below specific market impact levels (0.25%, 0.50%, 1.00%, 2.50%, 5.00%);
// lookahead + volume : estimates the market impact over specific lookaheads (one hour, eight hours, one day) if specific volumes are traded ($100k, $500k, $1m, $5m, $10m).
// lookahead + impact : estimates the maximum volume over specific lookaheads (one hour, eight hours, one day) to be traded while staying below specific market impact levels (0.25%, 0.50%, 1.00%, 2.50%, 5.00%);
//see: https://docs.trade.sheeldmatch.com/#get-market-impact
func (s *SheeldMarket) GetMarketImpact(val GetMarketImpactRequest) (*GetMarketImpactResponse, error) {
	path := s.baseUrl + "analytics/market-impact"
	u, _ := url.Parse(path)
	q := u.Query()
	q.Set("instrument", val.Instrument)
	if val.Impact != nil {
		q.Set("impact", fmt.Sprintf("%f", *val.Impact))
	}
	if val.Volume != nil {
		q.Set("volume", fmt.Sprintf("%f", *val.Volume))
	}
	if val.Lookahead != nil {
		q.Set("lookahead", *val.Lookahead)
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetMarketImpactResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}
