package sheeldmarket

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

//GetMPIDBalances This route allows market participants to retrieve their balances.
//see: https://docs.trade.sheeldmatch.com/#get-mpid-balances
func (s *SheeldMarket) GetMPIDBalances() ([]Balance, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"participants/"+s.cnf.MPID+"/balances", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := make([]Balance, 0)
	err = s.do(req, &resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetUserInformation This route allows users to retrive the detailed user data such as which market participants and accounts they have access to.
//see: https://docs.trade.sheeldmatch.com/#get-user-information
func (s *SheeldMarket) GetUserInformation() (*User, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"user", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(User)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetDepositAddresses This route allows market participants to retrieve the deposit addresses for the specified Account.
//see: https://docs.trade.sheeldmatch.com/#get-deposit-addresses
func (s *SheeldMarket) GetDepositAddresses(accountID string) ([]DepositAddress, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"accounts/"+accountID+"/assets/deposit-destinations", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := make([]DepositAddress, 0)
	err = s.do(req, &resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetWithdrawalAddresses This route allows market participants to retrieve the withdrawal addresses for the specified Account.
//see: https://docs.trade.sheeldmatch.com/#get-withdrawal-addresses
func (s *SheeldMarket) GetWithdrawalAddresses(accountID string) ([]WithdrawalAddress, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"accounts/"+accountID+"/assets/external-addresses", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := make([]WithdrawalAddress, 0)
	err = s.do(req, &resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//RequestWithdrawal This route allows market participants to request a withdrawal. The assets will be withdrawn from the Funding Account towards an external address.
//see: https://docs.trade.sheeldmatch.com/#request-a-withdrawal
func (s *SheeldMarket) RequestWithdrawal(accountID string, val RequestWithdrawalRequest) (*RequestWithdrawalResponse, error) {
	body, err := json.Marshal(val)
	if err != nil {
		return nil, fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"accounts/"+accountID+"/withdrawal-requests", bytes.NewBuffer(body))
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(RequestWithdrawalResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetWithdrawalRequests This route allows accounts to retrieve their withdrawal requests history.
//see: https://docs.trade.sheeldmatch.com/#get-withdrawal-requests
func (s *SheeldMarket) GetWithdrawalRequests(accountID string) (*GetWithdrawalRequestsResponse, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"accounts/"+accountID+"/withdrawal-requests", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetWithdrawalRequestsResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetDepositsHistory This route allows accounts to retrieve theirs deposit history.
//see: https://docs.trade.sheeldmatch.com/#get-deposits-history
func (s *SheeldMarket) GetDepositsHistory(accountID string) (*GetDepositsHistoryResponse, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"accounts/"+accountID+"/deposits", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetDepositsHistoryResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetWithdrawalsHistory This route allows accounts to retrieve their withdrawal history (not to be confused with the history for withdrawal requests).
//see: https://docs.trade.sheeldmatch.com/#get-withdrawals-history
func (s *SheeldMarket) GetWithdrawalsHistory(accountID string) (*GetWithdrawalsHistoryResponse, error) {
	// TODO pagination
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"accounts/"+accountID+"/withdrawals", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetWithdrawalsHistoryResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//RequestTransfer This route allows market participants to request a transfer. The assets will be withdrawn from source towards the destination.
//see: https://docs.trade.sheeldmatch.com/#request-a-transfer
func (s *SheeldMarket) RequestTransfer(accountID string, val RequestTransferRequest) (*RequestTransferResponse, error) {
	body, err := json.Marshal(val)
	if err != nil {
		return nil, fmt.Errorf("Marshal: %w", err)
	}
	req, err := http.NewRequest(http.MethodPost, s.baseUrl+"accounts/"+accountID+"/transfer-requests", bytes.NewBuffer(body))
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(RequestTransferResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}

//GetTransfersHistory This route allows accounts to retrieve theirs transfer requests history.
//see: https://docs.trade.sheeldmatch.com/#get-transfers-history
func (s *SheeldMarket) GetTransfersHistory(accountID string) (*GetTransfersHistoryResponse, error) {
	// TODO pagination
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"accounts/"+accountID+"/transfer-requests", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	err = s.authenticate(req)
	if err != nil {
		return nil, fmt.Errorf("authenticate: %w", err)
	}
	resp := new(GetTransfersHistoryResponse)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}
