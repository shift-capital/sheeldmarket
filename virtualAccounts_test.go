package sheeldmarket

import "errors"

func testGetAllVirtualAccounts(s *SheeldMarket, accountID string, email *string) error {
	res, err := s.GetAllVirtualAccounts(accountID, email)
	if err != nil {
		return errors.New("GetAllVirtualAccounts: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllVirtualAccounts() res == nil")
	}
	return nil
}

func testGetVirtualAccountInformation(s *SheeldMarket, accountID, virtualAccountID string) error {
	res, err := s.GetVirtualAccountInformation(accountID, virtualAccountID)
	if err != nil {
		return errors.New("GetVirtualAccountInformation: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountInformation() res == nil")
	}
	return nil
}

func testAddWithdrawalAddress(s *SheeldMarket, accountID, virtualAccountID string, val AddWithdrawalAddressRequest) error {
	err := s.AddWithdrawalAddress(accountID, virtualAccountID, val)
	if err != nil {
		return errors.New("AddWithdrawalAddress: " + err.Error())
	}
	return nil
}

func testGetAllWithdrawalAddresses(s *SheeldMarket, accountID string, assetID *string) error {
	res, err := s.GetAllWithdrawalAddresses(accountID, assetID)
	if err != nil {
		return errors.New("GetAllWithdrawalAddresses: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllWithdrawalAddresses() res == nil")
	}
	return nil
}

func testGetSpecificVirtualAccountWithdrawalAddresses(s *SheeldMarket, accountID, virtualAccountID string, assetID *string) error {
	res, err := s.GetSpecificVirtualAccountWithdrawalAddresses(accountID, virtualAccountID, assetID)
	if err != nil {
		return errors.New("GetSpecificVirtualAccountWithdrawalAddresses: " + err.Error())
	}
	if res == nil {
		return errors.New("GetSpecificVirtualAccountWithdrawalAddresses() res == nil")
	}
	return nil
}

func testGetAllDepositAddresses(s *SheeldMarket, accountID string, assetID *string) error {
	res, err := s.GetAllDepositAddresses(accountID, assetID)
	if err != nil {
		return errors.New("GetAllDepositAddresses: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllDepositAddresses() res == nil")
	}
	return nil
}

func testGetSpecificVirtualAccountDepositAddresses(s *SheeldMarket, accountID, virtualAccountID string, assetID *string) error {
	res, err := s.GetSpecificVirtualAccountDepositAddresses(accountID, virtualAccountID, assetID)
	if err != nil {
		return errors.New("GetSpecificVirtualAccountDepositAddresses: " + err.Error())
	}
	if res == nil {
		return errors.New("GetSpecificVirtualAccountDepositAddresses() res == nil")
	}
	return nil
}

func testRequestTransferFromVirtualAccount(s *SheeldMarket, accountID, virtualAccountID string, val RequestTransferFromVirtualAccountRequest) error {
	res, err := s.RequestTransferFromVirtualAccount(accountID, virtualAccountID, val)
	if err != nil {
		return errors.New("RequestTransferFromVirtualAccount: " + err.Error())
	}
	if res == nil {
		return errors.New("RequestTransferFromVirtualAccount() res == nil")
	}
	return nil
}

func testGetAllTransferRequests(s *SheeldMarket, accountID string, assetID *string) error {
	res, err := s.GetAllTransferRequests(accountID, assetID)
	if err != nil {
		return errors.New("GetAllTransferRequests: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllTransferRequests() res == nil")
	}
	return nil
}

func testGetVirtualAccountTransferRequests(s *SheeldMarket, accountID, virtualAccountID string, assetID *string) error {
	res, err := s.GetVirtualAccountTransferRequests(accountID, virtualAccountID, assetID)
	if err != nil {
		return errors.New("GetVirtualAccountTransferRequests: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountTransferRequests() res == nil")
	}
	return nil
}

func testGetTransferRequest(s *SheeldMarket, accountID, virtualAccountID, transferRequestID string) error {
	res, err := s.GetTransferRequest(accountID, virtualAccountID, transferRequestID)
	if err != nil {
		return errors.New("GetTransferRequest: " + err.Error())
	}
	if res == nil {
		return errors.New("GetTransferRequest() res == nil")
	}
	return nil
}

func testRequestWithdrawalFromVirtualAccount(s *SheeldMarket, accountID, virtualAccountID string, val RequestWithdrawalFromVirtualAccountRequest) error {
	res, err := s.RequestWithdrawalFromVirtualAccount(accountID, virtualAccountID, val)
	if err != nil {
		return errors.New("RequestWithdrawalFromVirtualAccount: " + err.Error())
	}
	if res == nil {
		return errors.New("RequestWithdrawalFromVirtualAccount() res == nil")
	}
	return nil
}

func testGetAllWithdrawalRequests(s *SheeldMarket, accountID string, status *VirtualAccountWithdrawalStatus, assetID *string) error {
	res, err := s.GetAllWithdrawalRequests(accountID, status, assetID)
	if err != nil {
		return errors.New("GetAllWithdrawalRequests: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllWithdrawalRequests() res == nil")
	}
	return nil
}

func testGetVirtualAccountWithdrawalRequests(s *SheeldMarket, accountID, virtualAccountID string, status *VirtualAccountWithdrawalStatus, assetID *string) error {
	res, err := s.GetVirtualAccountWithdrawalRequests(accountID, virtualAccountID, status, assetID)
	if err != nil {
		return errors.New("GetVirtualAccountWithdrawalRequests: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountWithdrawalRequests() res == nil")
	}
	return nil
}

func testGetWithdrawalRequest(s *SheeldMarket, accountID, virtualAccountID, withdrawalRequestID string) error {
	res, err := s.GetWithdrawalRequest(accountID, virtualAccountID, withdrawalRequestID)
	if err != nil {
		return errors.New("GetWithdrawalRequest: " + err.Error())
	}
	if res == nil {
		return errors.New("GetWithdrawalRequest() res == nil")
	}
	return nil
}

func testGetAllOrders(s *SheeldMarket, accountID string, val GetOrdersRequest) error {
	res, err := s.GetAllOrders(accountID, val)
	if err != nil {
		return errors.New("GetAllOrders: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllOrders() res == nil")
	}
	return nil
}

func testGetVirtualAccountOrders(s *SheeldMarket, accountID, virtualAccountId string, val GetOrdersRequest) error {
	res, err := s.GetVirtualAccountOrders(accountID, virtualAccountId, val)
	if err != nil {
		return errors.New("GetVirtualAccountOrders: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountOrders() res == nil")
	}
	return nil
}

func testGetVirtualAccountOrder(s *SheeldMarket, accountID string, val GetVirtualAccountOrderRequest) error {
	res, err := s.GetVirtualAccountOrder(accountID, val)
	if err != nil {
		return errors.New("GetVirtualAccountOrder: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountOrder() res == nil")
	}
	return nil
}

func testGetAllVirtualAccountsBalances(s *SheeldMarket, accountID string, val GetAllVirtualAccountsBalancesRequest) error {
	res, err := s.GetAllVirtualAccountsBalances(accountID, val)
	if err != nil {
		return errors.New("GetAllVirtualAccountsBalances: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllVirtualAccountsBalances() res == nil")
	}
	return nil
}

func testGetVirtualAccountBalances(s *SheeldMarket, accountID, virtualAccountID string, val GetVirtualAccountBalancesRequest) error {
	res, err := s.GetVirtualAccountBalances(accountID, virtualAccountID, val)
	if err != nil {
		return errors.New("GetVirtualAccountBalances: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountBalances() res == nil")
	}
	return nil
}

func testGetAllVirtualAccountsActivity(s *SheeldMarket, accountID string, val GetVirtualAccountActivityRequest) error {
	res, err := s.GetAllVirtualAccountsActivity(accountID, val)
	if err != nil {
		return errors.New("GetAllVirtualAccountsActivity: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllVirtualAccountsActivity() res == nil")
	}
	return nil
}

func testGetVirtualAccountActivity(s *SheeldMarket, accountID, virtualAccountId string, val GetVirtualAccountActivityRequest) error {
	res, err := s.GetVirtualAccountActivity(accountID, virtualAccountId, val)
	if err != nil {
		return errors.New("GetVirtualAccountActivity: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountActivity() res == nil")
	}
	return nil
}

func testGetAllVirtualAccountsTrades(s *SheeldMarket, accountID string, val GetVirtualAccountsTradesRequest) error {
	res, err := s.GetAllVirtualAccountsTrades(accountID, val)
	if err != nil {
		return errors.New("GetAllVirtualAccountsTrades: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllVirtualAccountsTrades() res == nil")
	}
	return nil
}

func testGetVirtualAccountTrades(s *SheeldMarket, accountID, virtualAccountId string, val GetVirtualAccountsTradesRequest) error {
	res, err := s.GetVirtualAccountTrades(accountID, virtualAccountId, val)
	if err != nil {
		return errors.New("GetVirtualAccountTrades: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountTrades() res == nil")
	}
	return nil
}

func testGetAllWithdrawals(s *SheeldMarket, accountID string, val GetAllWithdrawalsRequest) error {
	res, err := s.GetAllWithdrawals(accountID, val)
	if err != nil {
		return errors.New("GetAllWithdrawals: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllWithdrawals() res == nil")
	}
	return nil
}

func testGetVirtualAccountWithdrawals(s *SheeldMarket, accountID, virtualAccountId string, val GetAllWithdrawalsRequest) error {
	res, err := s.GetVirtualAccountWithdrawals(accountID, virtualAccountId, val)
	if err != nil {
		return errors.New("GetVirtualAccountWithdrawals: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountWithdrawals() res == nil")
	}
	return nil
}

func testGetAllDeposits(s *SheeldMarket, accountID string, val GetAllDepositsRequest) error {
	res, err := s.GetAllDeposits(accountID, val)
	if err != nil {
		return errors.New("GetAllDeposits: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllDeposits() res == nil")
	}
	return nil
}

func testGetVirtualAccountDeposits(s *SheeldMarket, accountID, virtualAccountId string, val GetAllDepositsRequest) error {
	res, err := s.GetVirtualAccountDeposits(accountID, virtualAccountId, val)
	if err != nil {
		return errors.New("GetVirtualAccountDeposits: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountDeposits() res == nil")
	}
	return nil
}

func testDepositFundsToEarningAccount(s *SheeldMarket, accountID, virtualAccountId string, val EarningAccountMovementRequest) error {
	res, err := s.DepositFundsToEarningAccount(accountID, virtualAccountId, val)
	if err != nil {
		return errors.New("DepositFundsToEarningAccount: " + err.Error())
	}
	if res == nil {
		return errors.New("DepositFundsToEarningAccount() res == nil")
	}
	return nil
}

func testWithdrawFundsFromEarningAccount(s *SheeldMarket, accountID, virtualAccountId string, val EarningAccountMovementRequest) error {
	res, err := s.WithdrawFundsFromEarningAccount(accountID, virtualAccountId, val)
	if err != nil {
		return errors.New("WithdrawFundsFromEarningAccount: " + err.Error())
	}
	if res == nil {
		return errors.New("WithdrawFundsFromEarningAccount() res == nil")
	}
	return nil
}

func testGetAllVirtualAccountsEarnRequests(s *SheeldMarket, accountID string, val GetVirtualAccountsEarnRequests) error {
	res, err := s.GetAllVirtualAccountsEarnRequests(accountID, val)
	if err != nil {
		return errors.New("GetAllVirtualAccountsEarnRequests: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllVirtualAccountsEarnRequests() res == nil")
	}
	return nil
}

func testGetVirtualAccountEarnRequests(s *SheeldMarket, accountID, virtualAccountId string, val GetVirtualAccountsEarnRequests) error {
	res, err := s.GetVirtualAccountEarnRequests(accountID, virtualAccountId, val)
	if err != nil {
		return errors.New("GetVirtualAccountEarnRequests: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountEarnRequests() res == nil")
	}
	return nil
}

func testGetEarnRequest(s *SheeldMarket, accountID, virtualAccountId, earnRequestID string, val GetVirtualAccountOrderRequest) error {
	res, err := s.GetEarnRequest(accountID, virtualAccountId, earnRequestID)
	if err != nil {
		return errors.New("GetEarnRequest: " + err.Error())
	}
	if res == nil {
		return errors.New("GetEarnRequest() res == nil")
	}
	return nil
}

func testGetAllVirtualAccountsEarningInterests(s *SheeldMarket, accountID string) error {
	res, err := s.GetAllVirtualAccountsEarningInterests(accountID)
	if err != nil {
		return errors.New("GetAllVirtualAccountsEarningInterests: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAllVirtualAccountsEarningInterests() res == nil")
	}
	return nil
}

func testGetVirtualAccountEarningInterests(s *SheeldMarket, accountID, virtualAccountId string) error {
	res, err := s.GetVirtualAccountEarningInterests(accountID, virtualAccountId)
	if err != nil {
		return errors.New("GetVirtualAccountEarningInterests: " + err.Error())
	}
	if res == nil {
		return errors.New("GetVirtualAccountEarningInterests() res == nil")
	}
	return nil
}
