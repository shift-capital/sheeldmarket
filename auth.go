package sheeldmarket

import (
	"encoding/base64"
	"fmt"
	"net/http"
)

//GenerateAuthToken
// Market participants can retrieve tokens using their credentials and following the BASIC authentication scheme.
//
// This scheme consists in a HTTP request with the Authorization header that contains the word Basic followed by the base64-encoded string email:password.
//
//see: https://docs.trade.sheeldmatch.com/#generate-a-token
func (s *SheeldMarket) GenerateAuthToken() (*AuthToken, error) {
	req, err := http.NewRequest(http.MethodGet, s.baseUrl+"auth/signin", nil)
	if err != nil {
		return nil, fmt.Errorf("NewRequest: %w", err)
	}
	credentials := s.cnf.Username + ":" + s.cnf.Password
	encodedCredentials := base64.StdEncoding.EncodeToString([]byte(credentials))
	// encodedCredentials = base64.b64encode(credentials.encode("utf-8"))
	req.Header.Set("Authorization", "Basic "+encodedCredentials)
	resp := new(AuthToken)
	err = s.do(req, resp)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}
	return resp, nil
}
