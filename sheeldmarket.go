package sheeldmarket

import (
	"fmt"
	"net/http"
)

type IConfig interface {
	Username() (string, error)
	Password() (string, error)
	MPID() (string, error)
	IsProduction() bool
}

type SheeldMarket struct {
	cnf             *SheeldMarketConfig
	client          *http.Client
	baseUrl         string
	wsMarketDataURL string
	wsPriceFeedURL  string
}

func NewSheeldMarket(cnf *SheeldMarketConfig) (*SheeldMarket, error) {
	s := &SheeldMarket{
		cnf:             cnf,
		wsMarketDataURL: "wss://sheeldmatch.com:6443/",
		wsPriceFeedURL:  "wss://sheeldmatch.com:5443/",
	}

	s.client = &http.Client{}
	if cnf.Test {
		s.baseUrl = "https://uat.sheeldmatch.com/api/"
	} else {
		s.baseUrl = "https://sheeldmatch.com/api/"
	}

	return s, nil
}

//GetSheeldMarket ...
func GetSheeldMarket(cnf IConfig, tenantName string) (*SheeldMarket, error) {
	username, err := cnf.Username()
	if err != nil {
		return nil, fmt.Errorf("Username: %w", err)
	}
	password, err := cnf.Password()
	if err != nil {
		return nil, fmt.Errorf("Username: %w", err)
	}
	mpid, err := cnf.MPID()
	if err != nil {
		return nil, fmt.Errorf("Username: %w", err)
	}
	return NewSheeldMarket(&SheeldMarketConfig{
		Test:     !cnf.IsProduction(),
		Username: username,
		Password: password,
		MPID:     mpid,
	})
}
