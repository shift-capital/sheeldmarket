package sheeldmarket

import "errors"

func testAddDMAOrder(s *SheeldMarket, orderType DMAOrderType, val AddDMAOrderRequest) error {
	err := s.AddDMAOrder(orderType, val)
	if err != nil {
		return errors.New("AddDMAOrder: " + err.Error())
	}
	return nil
}

func testAddSOROrder(s *SheeldMarket, orderType SOROrderType, val AddSOROrderRequest) error {
	err := s.AddSOROrder(orderType, val)
	if err != nil {
		return errors.New("AddSOROrder: " + err.Error())
	}
	return nil
}

func testAddDSAOrder(s *SheeldMarket, orderType DSAOrderType, val AddDSAOrderRequest) error {
	err := s.AddDSAOrder(orderType, val)
	if err != nil {
		return errors.New("AddADSAOrder: " + err.Error())
	}
	return nil
}

func testAddTMOOrder(s *SheeldMarket, orderType TMOOrderType, val AddTMOOrderRequest) error {
	err := s.AddTMOOrder(orderType, val)
	if err != nil {
		return errors.New("AddATMOOrder: " + err.Error())
	}
	return nil
}

func testModifyOrder(s *SheeldMarket, token string, val ModifyOrderRequest) error {
	err := s.ModifyOrder(token, val)
	if err != nil {
		return errors.New("ModifyOrder: " + err.Error())
	}
	return nil
}

func testReduceOrder(s *SheeldMarket, token string, val ReduceOrderRequest) error {
	err := s.ReduceOrder(token, val)
	if err != nil {
		return errors.New("ReduceOrder: " + err.Error())
	}
	return nil
}

func testGetOrderStatus(s *SheeldMarket, token string) error {
	res, err := s.GetOrderStatus(token)
	if err != nil {
		return errors.New("GetOrderStatus: " + err.Error())
	}
	if res == nil {
		return errors.New("GetOrderStatus() res == nil")
	}
	return nil
}

func testGetOpenOrders(s *SheeldMarket) error {
	res, err := s.GetOpenOrders()
	if err != nil {
		return errors.New("GetOpenOrders: " + err.Error())
	}
	if res == nil {
		return errors.New("GetOpenOrders() res == nil")
	}
	return nil
}

func testGetOrders(s *SheeldMarket, val GetOrdersRequest) error {
	res, err := s.GetOrders(val)
	if err != nil {
		return errors.New("GetOrders: " + err.Error())
	}
	if res == nil {
		return errors.New("GetOrders() res == nil")
	}
	return nil
}

func testCancelOrder(s *SheeldMarket, token string) error {
	err := s.CancelOrder(token)
	if err != nil {
		return errors.New("CancelOrder: " + err.Error())
	}
	return nil
}

func testGetExecutions(s *SheeldMarket) error {
	res, err := s.GetExecutions()
	if err != nil {
		return errors.New("GetExecutions: " + err.Error())
	}
	if res == nil {
		return errors.New("GetExecutions() res == nil")
	}
	return nil
}

func testMassCancelOrders(s *SheeldMarket) error {
	err := s.MassCancelOrders()
	if err != nil {
		return errors.New("MassCancelOrders: " + err.Error())
	}
	return nil
}

func testSuspendTrading(s *SheeldMarket) error {
	err := s.SuspendTrading()
	if err != nil {
		return errors.New("SuspendTrading: " + err.Error())
	}
	return nil
}

func testResumeTrading(s *SheeldMarket) error {
	err := s.ResumeTrading()
	if err != nil {
		return errors.New("ResumeTrading: " + err.Error())
	}
	return nil
}
