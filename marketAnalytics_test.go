package sheeldmarket

import "errors"

func testGetAnalyticUniverse(s *SheeldMarket) error {
	res, err := s.GetAnalyticUniverse()
	if err != nil {
		return errors.New("GetAnalyticUniverse: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAnalyticUniverse() res == nil")
	}
	return nil
}

func testGetTradesAverageSize(s *SheeldMarket, val AnalyticParameters) error {
	res, err := s.GetTradesAverageSize(val)
	if err != nil {
		return errors.New("GetTradesAverageSize: " + err.Error())
	}
	if res == nil {
		return errors.New("GetTradesAverageSize() res == nil")
	}
	return nil
}

func testGetDailyAverageVolume(s *SheeldMarket, val AnalyticParameters) error {
	res, err := s.GetDailyAverageVolume(val)
	if err != nil {
		return errors.New("GetDailyAverageVolume: " + err.Error())
	}
	if res == nil {
		return errors.New("GetDailyAverageVolume() res == nil")
	}
	return nil
}

func testGetMoneyFlow(s *SheeldMarket, val AnalyticParameters) error {
	res, err := s.GetMoneyFlow(val)
	if err != nil {
		return errors.New("GetMoneyFlow: " + err.Error())
	}
	if res == nil {
		return errors.New("GetMoneyFlow() res == nil")
	}
	return nil
}

func testGetAnualizedVolatility(s *SheeldMarket, val AnalyticParameters) error {
	res, err := s.GetAnualizedVolatility(val)
	if err != nil {
		return errors.New("GetAnualizedVolatility: " + err.Error())
	}
	if res == nil {
		return errors.New("GetAnualizedVolatility() res == nil")
	}
	return nil
}

func testGetEstimatedSeasonality(s *SheeldMarket, val AnalyticParameters) error {
	res, err := s.GetEstimatedSeasonality(val)
	if err != nil {
		return errors.New("GetEstimatedSeasonality: " + err.Error())
	}
	if res == nil {
		return errors.New("GetEstimatedSeasonality() res == nil")
	}
	return nil
}

func testGetMarketImpact(s *SheeldMarket, val GetMarketImpactRequest) error {
	res, err := s.GetMarketImpact(val)
	if err != nil {
		return errors.New("GetMarketImpact: " + err.Error())
	}
	if res == nil {
		return errors.New("GetMarketImpact() res == nil")
	}
	return nil
}
